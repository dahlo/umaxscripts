#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <regions gtf-file> <output file> <annotation file1> ... <annotation fileN>
# ex.
# perl scriptname.pl regions.bed refSeq.bed ensembl.bed ncRna.bed

use warnings;
use strict;

=pod

Used to annotate the given bed or gtf file with the provided bed annotation files.

=cut

# define messages
my $usage = "annotateBedFiles.pl usage:  perl scriptname.pl <regions file> <output file> <annotation file1> ... <annotation fileN>\n";






########################################
####### PATH TO THE SCRIPTS ############
########################################
# make sure this is correct..
my $scripts = "/bubo/home/h18/dahlo/glob/work/uppmaxScripts/bedtools";









# get the file names
my $regions = shift or die $usage;
my $out = shift or die $usage;
my @annos = @ARGV or die $usage;



# create a timestamp
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
$year += 1900; # to make it y2k compatible (seriously, how lame is that? :)  )
# add zero to all one-digit numbers  (9 -> 09)
$year = sprintf("%.2d", $year);
$mon = sprintf("%.2d", $mon+1); # adjust to real month, not array index
$mday = sprintf("%.2d", $mday);
$hour = sprintf("%.2d", $hour);
$min = sprintf("%.2d", $min);
$sec = sprintf("%.2d", $sec);
my $stamp = "$year$mon$mday$hour$min$sec";
#print "$stamp\n$year\t$mon\t$mday\t$hour\t$min\t$sec\n";


# build command to merge annotation files
my $cmd = "cat ";
for(my $i = 0; $i <= $#ARGV; $i++){
  $cmd .= "$ARGV[$i] ";
  
}
$cmd .= "> $out.$stamp.tmp";

# execute command
system($cmd);


# start the R script
#system("R CMD BATCH --no-save --no-restore '--args $regions $out $out.$stamp.tmp' $scripts/annotateBedFiles.r $out.annotateBedFiles.Rout");



# clean up
#system("rm $out.$stamp.tmp");
