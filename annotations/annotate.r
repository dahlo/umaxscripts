# First read in the arguments listed at the command line
args=(commandArgs(TRUE))

# args is now a list of character vectors
# check to see if arguments are passed.
if(length(args)==0){
    print("No arguments supplied.")
}

# get the arguments
region = args[1]
out = args[2]
anno = args[3]



# for debugging only
if(TRUE){
  rm(list=ls())
  gc()
  region = '/bubo/home/h18/dahlo/glob/work/projects/10af_annotationsProject/test/0mocktranscripts.gtf'
  out = '/bubo/home/h18/dahlo/glob/work/projects/10af_annotationsProject/test/test.out'
  anno = '/bubo/home/h18/dahlo/glob/work/projects/10af_annotationsProject/test/0refSeq.hg19.gtf'
}


# Define which columns to read
columnList=list('character',NULL,'character','integer','integer',NULL,'character',NULL,'character')

# open the files
regions = read.csv(file=region,sep='\t',colClasses=columnList,header=F)
colnames(regions) = c("chr","type","start","end","strand","id")

annos = read.csv(file=anno,sep='\t',colClasses=columnList,header=F)
colnames(annos) = c("chr","type","start","end","strand","id")

# filter out the transcript ids
for(i in 1:dim(regions)[1]){
  
  ### get the transcript id from the id field
  
}


# sort the files (end, start, chr) Hmm, should make sure the mitochondria is not named differently in the files... That would properly mess things up..
regions = regions[order(regions$end),]
regions = regions[order(regions$start),]
regions = regions[order(regions$chr),]
annos = annos[order(annos$end),]
annos = annos[order(annos$start),]
annos = annos[order(annos$chr),]

# convert to faster data structures for the loops
regionsChr = regions$chr
regionsStart = regions$start
regionsEnd = regions$end
regionsId = regions$id

annosChr = annos$chr
annosStart = annos$start
annosEnd = annos$end
annosId = annos$id


# separate the transcripts from the exons


# For each transcript, store all exons belonging to it


# do the overlap search on the transcripts


# For overlapping transcripts, measure the overlap of their exons


# calculate an overlap score (intersection / union?) for each transcript based on the exon overlaps























resChr = vector()
resStart = vector()
resEnd = vector()
resRid = vector()
resAid = vector()
resClass = vector()
res = vector()
resChr = vector()
resChr = vector()


# alrighty, go throug all the regions
annopos = 1
respos = 1
for(i = 1:dim(regions)[1]){
  if(){ # if the annotations are non overlapping, and BEFORE the region
  
    # take a step forward in the annotations
    annopos = annopos + 1
  
  
  } else if(){ # if the annotations are non overlapping, and AFTER the region
  
  
    # do nothing (and let the rest of the script save the result and go to the next region)
    
  
  
  } else{ # if the annotation some how overlaps
  
    # reset
    current = regions[i,1]
    current[2] = regions[i,2]
    current[3] = regions[i,3]
    current[4] = regions[i,4]
    
    # start the inner loop
    run = TRUE
    peek = 0
    while(run){
    
      # check if the next annotation also overlaps the region
      if(){ # if it does overlap
      
        # calculate stuff
        res
        # res[respos,1] = regions[i,1]
        # res[respos,2] = regions[i,2]
        # res[respos,3] = regions[i,3]
        # res[respos,4] = regions[i,4]
        # res[respos,5] = anno$id[annopos + peek]
        # res[respos,6] = overlap size perhaps?
        # res[respos,7] = percentage of region overlap?
        # res[respos,8] = percentage of annotation overlap?
        # res[respos,9] = average stuff?
        
        respos = respos + 1
      
      
      } else{ # if it doesn't
      
        # end the loop
        run = FALSE
      
      }
    
    }
    
    # save the results
    # regions[i,5] = current[1]
    # regions[i,6] = current[2]
    # regions[i,7] = current[3]
    
  }

}
