#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl compareAllAnnovar.pl <in directory> <out directory>
# ex.
# perl compareAllAnnovar.pl /path/to/annovarFiles/ out/

use warnings;
use strict;
use File::Path;
use Cwd;

=pod
This script will 

Pseudo code:
* 
=cut


# define messages
my $usage = "Usage: perl compareAllAnnovar.pl <in directory> <out directory>\n";

# the location of the annotations and annovar
my $scripts = "/bubo/home/h18/dahlo/glob/work/uppmaxScripts"; # why is this not in the path in perl


# get the file names
my $infolder = shift or die $usage;
my $outfolder = shift or die $usage;
$infolder = absPath(remSlash($infolder));
$outfolder = absPath(remSlash($outfolder));
mkpath($outfolder);

# List all the files in the infolder
open(LS,"find $infolder -maxdepth 1 -type f |") || die "Failed: $!\n";

# get all the files
my @files;
while(<LS>){
  # remove the newline
  chomp($_);
  push(@files,$_);
  print "$_\n";
}

# process the files
my $j;
for(my $i = 0; $i <= $#files; $i++){
  for(my $j = $i+1; $j <= $#files; $j++){
    my ($file1,$path1) = sepPath($files[$i]);
    my ($file2,$path2) = sepPath($files[$j]);
    system("perl $scripts/annovar/compareAnnovar.pl $files[$i] $files[$j] $outfolder/$file1.$file2.compare"); 
    print "$file1.$file2.compare\n";
  }
}



### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (path/to,file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}

sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}

sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}



# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
