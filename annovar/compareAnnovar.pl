#!/usr/bin/perl
# Author: Martin Dahlo
# Compare two annovar files with eachother
# Usage: perl compareAnnovar.pl <file 1> <file 2> <result prefix>

use warnings;
use strict;

my $usage = "Usage: perl compareAnnovar.pl <file 1> <file 2> <result prefix>\n";

# Get in file and out folder
my $list1 = shift or die $usage;
my $list2 = shift or die $usage;
my $prefix = shift or die $usage;

my @filename = split(/\//,$list1);
my $list1Name = pop(@filename);
@filename = split(/\//,$list2);
my $list2Name = pop(@filename);


# Open the files
open L1, "<", $list1 or die $!;
open L2, "<", $list2 or die $!;
open INTER, ">", "$prefix.intersect" or die $!;
open L1U, ">", "$prefix.$list1Name.unique" or die $!;
open L2U, ">", "$prefix.$list2Name.unique" or die $!;
open STAT, ">", "$prefix.stats" or die $!;

# print headers
print INTER "# $list1Name\n# $list2Name\n";
print L1U "# $list1Name\n";
print L2U "# $list2Name\n";

# initiate variabless
my %list1;
my %list2;
my @inter;
my @l1;
my @l2;
my $l1Tot = 0;
my $l2Tot = 0;

# read list1
while(<L1>){
	chomp($_);
  $_ =~ m/^(\d+\t\d+)/;
  $list1{$1} = $_;
  $l1Tot++;
}

# read list2
while(<L2>){
	chomp($_);
  $_ =~ m/^(\d+\t\d+)/;
  $list2{$1} = $_;
  $l2Tot++;
}

# go through list1
foreach my $key (sort keys %list1){
  if(defined $list2{$key}){ # does it exist in both lists?
    push(@inter,"$list1{$key}\n$list2{$key}\n"); # store both entries in the intersection file
  }else{
    push(@l1,"$list1{$key}\n"); # store the enrty in list1 unique file
  }
}

# go through list2
foreach my $key (sort keys %list2){
  if(defined $list1{$key}){ # does it exist in both lists?
    # do nothing
  }else{
    push(@l2,"$list2{$key}\n"); # store the enrty in list2 unique file
  }
}

# print results to file
for(my $i = 0; $i <= $#inter; $i++){
	print INTER $inter[$i];
}

for(my $i = 0; $i <= $#l1; $i++){
	print L1U $l1[$i];
}

for(my $i = 0; $i <= $#l2; $i++){
	print L2U $l2[$i];
}

# do some stats
my $l1Unique = $#l1;
my $l1UniquePer = 100 * $l1Unique / $l1Tot;
my $l1Inter = $l1Tot - $l1Unique;
my $l1InterPer = 100 * $l1Inter / $l1Tot;
my $l2Unique = $#l2;
my $l2UniquePer = 100 * $l2Unique / $l2Tot;
my $l2Inter = $l2Tot - $l2Unique;
my $l2InterPer = 100 * $l2Inter / $l2Tot;

my $intersect = $#inter;

print STAT <<EOF;
Comparison statistics:
Category\tCount\tPercent

$list1Name:
Total number of SNPs in list:   $l1Tot
Number of intersecting SNPs:    $l1Inter\t$l1InterPer
Number of unique SNPs:          $l1Unique\t$l1UniquePer

$list2Name:
Total number of SNPs in list:   $l2Tot
Number of intersecting SNPs:    $l2Inter\t$l2InterPer
Number of unique SNPs:          $l2Unique\t$l2UniquePer

Total number of intersecting SNPs:    $intersect
EOF
