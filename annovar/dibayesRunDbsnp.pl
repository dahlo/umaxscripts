#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl dibayesRunDbsnp.pl <gff3 file> <output prefix>
# ex.
# perl dibayesRunDbsnp.pl diBayes_SNP.gff3 140.4MM.Prog

use warnings;
use strict;

=pod
This script will run Annovar on a DiBayes GFF file

Pseudo code:
* convert the gff file to annovar format
* run annovar on it
=cut


# define messages
my $usage = "Usage: perl dibayesRunDbsnp.pl <gff3 file> <output prefix>\n";

# the location of the annotations and annovar
my $annos = "/bubo/home/h18/dahlo/glob/work/apps/annovar/humandb";
my $annovar = "/bubo/home/h18/dahlo/glob/work/apps/annovar"; # why is this not in the path in perl
my $scripts = "/bubo/home/h18/dahlo/glob/work/uppmaxScripts"; # why is this not in the path in perl


# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# convert the gff file to annovar format
system("perl $scripts/conversions/dibayes2annovar.pl $infile $outfile.annovar 1");

# run annovar on it, using  gene annotations
system("perl $annovar/annotate_variation.pl -buildver hg19 -geneanno $outfile.annovar $annos");
system("perl $annovar/annotate_variation.pl -buildver hg19 -filter -dbtype snp131 $outfile.annovar $annos");
system("perl $scripts/annovar/generateStats.pl $outfile hg19 snp131");
