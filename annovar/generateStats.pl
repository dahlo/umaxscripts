#!/usr/bin/perl
# Author: Anna Johansson, slightly modified by Martin Dahlo
#
# Usage:  perl scriptname.pl <annovar prefix> <build version> <dbSNP version>
# ex.
# perl scriptname.pl 1.4MM.Prog hg19 131

use warnings;
use strict;

=pod

=cut

my $usage = "Usage:  perl scriptname.pl <annovar prefix> <build version> <dbSNP version>\n";

# Argument should be the stub for the file that where run through annovar, the build ( hg19) and the dbSNP version (130, 131)

my $variants                          = $ARGV[0].".annovar.variant_function" or die $usage;
my $exonic_var                        = $ARGV[0].".annovar.exonic_variant_function" or die $usage;
my $buildver                          = $ARGV[1] or die $usage;
my $dbsnpver                          = $ARGV[2] or die $usage;
my $dbsnp_filtered                    = $ARGV[0].".annovar.".$buildver."_".$dbsnpver."_filtered";
my $outfile                           = $ARGV[0].".annovar.stat";
my $non_sym_gene_list                 = $ARGV[0].".non_sym_gene_list";			
my $sym_gene_list                     = $ARGV[0].".sym_gene_list";
my $non_sym_gene_list_not_in_dbsnp    = $ARGV[0].".non_sym_gene_list_not_in_dbsnp";
my $sym_gene_list_not_in_dbsnp        = $ARGV[0].".sym_gene_list_not_in_dbsnp";
my $stop_gain_gene_list               = $ARGV[0].".stop_gain_gene_list";
my $stop_gain_gene_list_not_in_dbsnp  = $ARGV[0].".stop_gain_gene_list_not_in_dbsnp";

open(V, $variants)                                || die "Couldn't open $variants";
open(E, $exonic_var)                              || die "Couldn't open $exonic_var";
open(SNP, $dbsnp_filtered)                        || die "Couldn't open $dbsnp_filtered";
open(O, ">".$outfile)                             || die "Couldn't open $outfile";
open(NS,">".$non_sym_gene_list )                  || die "Couldn't open $non_sym_gene_list";
open(S,">".$sym_gene_list )                       || die "Couldn't open $sym_gene_list";
open(NS_N,">".$non_sym_gene_list_not_in_dbsnp )   || die "Couldn't open $non_sym_gene_list";
open(S_N,">".$sym_gene_list_not_in_dbsnp )        || die "Couldn't open $sym_gene_list";
open(SG,">".$stop_gain_gene_list )                || die "Couldn't open $stop_gain_gene_list";
open(SG_N,">".$stop_gain_gene_list_not_in_dbsnp ) || die "Couldn't open $stop_gain_gene_list";

my %exonic_hash;
my %type_hash;

my $tot                    = 0;
my $not_dbsnp              = 0;
my $intron_tot		   = 0;
my $intergenic_tot         = 0;
my $ncRNA_tot              = 0;    
my $exon_tot               = 0;
my $exon_splice_tot        = 0;
my $syn_tot                = 0;
my $non_syn_tot            = 0;
my $stop_gain_tot          = 0;
my $downstream_tot         = 0;
my $upstream_tot           = 0;
my $UTR5_tot               = 0;
my $UTR3_tot               = 0;
my $intergenic_not_dbsnp   = 0;
my $intron_not_dbsnp       = 0;
my $exon_not_dbsnp         = 0;
my $exon_splice_not_dbsnp  = 0;
my $syn_not_dbsnp          = 0;
my $non_syn_not_dbsnp      = 0;
my $stop_gain_not_dbsnp    = 0;
my $ncRNA_not_dbsnp        = 0;    
my $coding_syn_not_dbsnp   = 0;
my $missense_not_dbsnp     = 0;
my $nonsense_not_dbsnp     = 0;
my $downstream_not_dbsnp   = 0;
my $upstream_not_dbsnp     = 0;
my $UTR5_not_dbsnp         = 0;
my $UTR3_not_dbsnp         = 0;


while(my $line =<E>) {
	$line =~/line(\d+)/;
	$exonic_hash{$1} = $line;
}

while(my $line = <V>) {
	$tot++;
	$line =~/\s+([1234567890XYabMT]+\s+\d+\s+\d+)/;
	my $string = $1;
	if ($line =~/intronic/)            { $intron_tot++;      $type_hash{$string} = "intronic"; }
	elsif ($line =~/intergenic/)       { $intergenic_tot++;  $type_hash{$string} = "intergenic"; }
	elsif ($line =~/ncRNA/)            { $ncRNA_tot++;       $type_hash{$string} = "ncRNA"; }
	elsif ($line =~/splicing/)         { $exon_splice_tot++; $type_hash{$string} = "exon_splice"; }
	elsif ($line =~/downstream/)       { $downstream_tot++;  $type_hash{$string} = "downstream"; }
	elsif ($line =~/upstream/)         { $upstream_tot++;    $type_hash{$string} = "upstream"; }
	elsif ($line =~/UTR5/)             { $UTR5_tot++;        $type_hash{$string} = "utr5"; }
	elsif ($line =~/UTR3/)             { $UTR3_tot++;        $type_hash{$string} = "utr3"; }	
	elsif ($line =~/exonic/)           { $exon_tot++; 
		if (defined $exonic_hash{$tot}) {
			if ( $exonic_hash{$tot} =~/line\d+\s+nonsynonymous\s+SNV\s+(.*)\s+[1234567890XYab]+\s+\d+\s+\d+/) { 
				$non_syn_tot++; 
				my $genes = $1;
				$type_hash{$string} = "exonic_non_syn".$genes; 
				print NS $genes."\n";
			} 
			elsif ($exonic_hash{$tot} =~/line\d+\s+synonymous\s+SNV\s+(.*)\s+[1234567890XYab]+\s+\d+\s+\d+/) { 
				$syn_tot++;  
				my $genes = $1;
				$type_hash{$string} = "exonic_syn".$genes;     
				print S $genes."\n";
			} 
			elsif ($exonic_hash{$tot} =~/line\d+\s+stopgain\s+SNV\s+(.*)\s+[1234567890XYab]+\s+\d+\s+\d+/) { 
				$stop_gain_tot++;  
				my $genes = $1;
				$type_hash{$string} = "exonic_stopgain".$genes;     
				print SG $genes."\n";
			} 			
		}
	}
	else { print $line;}
}

while(my $line = <SNP>) {
	$not_dbsnp++;
	$line =~/([1234567890XYabMT]+\s+\d+\s+\d+)/;
	my $string = $1;
	if (defined $type_hash{$string}) {
		if ($type_hash{$string} =~/intronic/)            { $intron_not_dbsnp++;      }
		elsif ($type_hash{$string} =~/intergenic/)       { $intergenic_not_dbsnp++;  }
		elsif ($type_hash{$string} =~/ncRNA/)            { $ncRNA_not_dbsnp++;      }
		elsif ($type_hash{$string} =~/exonic\;splicing/) { $exon_splice_not_dbsnp++; }
		elsif ($type_hash{$string} =~/downstream/)       { $downstream_not_dbsnp++; }
		elsif ($type_hash{$string} =~/upstream/)         { $upstream_not_dbsnp++; }
		elsif ($type_hash{$string} =~/UTR5/)             { $UTR5_not_dbsnp++; }
		elsif ($type_hash{$string} =~/UTR3/)             { $UTR3_not_dbsnp++; }
		elsif ($type_hash{$string} =~/exonic/)           { $exon_not_dbsnp++; 
			if ($type_hash{$string} =~/non_syn(.*)/) { 
				$non_syn_not_dbsnp++; 
				print NS_N $1."\n";
			} 
			elsif ($type_hash{$string} =~/syn(.*)/) { 
				$syn_not_dbsnp++; 
				print S_N $1."\n";
			} 
			elsif ($type_hash{$string} =~/stopgain(.*)/) { 
				$stop_gain_not_dbsnp++; 
				print SG_N $1."\n";
			} 
		}
	}
	else { print $line; }
}

print O "Kategori\t#\t%\n";
print O "Totalt antal:\t".$tot."\n";
print O "Antal INTE i dbSNP:\t".$not_dbsnp."\t".fix_format_fraction($not_dbsnp,$tot)."\n\n";
print O "Total # (fraction of total in %)\n";
print O "Antal intergenic:\t".$intergenic_tot."\t".fix_format_fraction($intergenic_tot,$tot)."\n";
print O "Antal i intron:\t".$intron_tot."\t".fix_format_fraction($intron_tot,$tot)."\n";
print O "Antal i ncRNA:\t".$ncRNA_tot."\t".fix_format_fraction($ncRNA_tot,$tot)."\n";    
print O "Antal i exon:\t".$exon_tot."\t".fix_format_fraction($exon_tot,$tot)."\n";
print O "Antal i exon splice:\t".$exon_splice_tot."\t".fix_format_fraction($exon_splice_tot,$tot)."\n";
print O "Antal exon, synonoumous:\t".$syn_tot."\t".fix_format_fraction($syn_tot,$tot)."\n";
print O "Antal exon, non-synonoumous:\t".$non_syn_tot."\t".fix_format_fraction($non_syn_tot,$tot)."\n";
print O "Antal exon, stop-gain:\t".$stop_gain_tot."\t".fix_format_fraction($stop_gain_tot,$tot)."\n";
print O "Antal downstream:\t".$downstream_tot."\t".fix_format_fraction($downstream_tot,$tot)."\n";
print O "Antal upstream:\t".$upstream_tot."\t".fix_format_fraction($upstream_tot,$tot)."\n";
print O "Antal UTR5:\t".$UTR5_tot."\t".fix_format_fraction($UTR5_tot,$tot)."\n";
print O "Antal UTR3:\t".$UTR3_tot."\t".fix_format_fraction($UTR3_tot,$tot)."\n\n";
print O "Total # of SV not in dbSNP (fraction of total not in dbSNP in %)\n";
print O "Antal intergenic not dbSNP:\t$intergenic_not_dbsnp\t".fix_format_fraction($intergenic_not_dbsnp,$not_dbsnp)."\n";
print O "Antal i intron not dbSNP:\t$intron_not_dbsnp\t".fix_format_fraction($intron_not_dbsnp,$not_dbsnp)."\n";
print O "Antal i ncRNA not dbSNP:\t$ncRNA_not_dbsnp\t".fix_format_fraction($ncRNA_not_dbsnp,$not_dbsnp)."\n";    
print O "Antal i exon not dbSNP:\t$exon_not_dbsnp\t".fix_format_fraction($exon_not_dbsnp,$not_dbsnp)."\n";
print O "Antal i exon splice not dbSNP:\t$exon_splice_not_dbsnp\t".fix_format_fraction($exon_splice_not_dbsnp,$not_dbsnp)."\n";
print O "Antal exon, synonoumous not dbSNP:\t$syn_not_dbsnp\t".fix_format_fraction($syn_not_dbsnp,$not_dbsnp)."\n";
print O "Antal exon, non-synonoumous not dbSNP:\t$non_syn_not_dbsnp\t".fix_format_fraction($non_syn_not_dbsnp,$not_dbsnp)."\n";
print O "Antal exon, stop-gain not dbSNP:\t$stop_gain_not_dbsnp\t".fix_format_fraction($stop_gain_not_dbsnp,$not_dbsnp)."\n";
print O "Antal downstream not dbSNP:\t$downstream_not_dbsnp\t".fix_format_fraction($downstream_not_dbsnp,$not_dbsnp)."\n";
print O "Antal upstream not dbSNP:\t$upstream_not_dbsnp\t".fix_format_fraction($upstream_not_dbsnp,$not_dbsnp)."\n";
print O "Antal UTR5 not dbSNP:\t$UTR5_not_dbsnp\t".fix_format_fraction($UTR5_not_dbsnp,$not_dbsnp)."\n";
print O "Antal UTR3 not dbSNP:\t$UTR3_not_dbsnp\t".fix_format_fraction($UTR3_not_dbsnp,$not_dbsnp)."\n";

sub fix_format_fraction {
	my $t = $_[0];
	my $n = $_[1];
  
  # in the unlikely event of one of the arguments being zero
  if($t == 0) { return 0; }
  if($n == 0) { return 100; }
  
  # in the unlikely event that the are equal
	if($t == $n) { return 100; }
  
  # 'convert' to percent
	return sprintf "%.2f", ($t / $n * 100);
  
}

