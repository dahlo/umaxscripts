#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl pileupRunDbsnp.pl <pileup file> <output prefix> <indels also, or only snps?  i/s>
# ex.
# perl pileupRunDbsnp.pl samtools.pileup 140.4MM.Prog i
# perl pileupRunDbsnp.pl samtools.pileup 140.4MM.Prog s

use warnings;
use strict;

=pod
This script will run Annovar on a samtools pileup file

Pseudo code:
* convert the pileup file to annovar format
* run annovar on it
=cut


# define messages
my $usage = "Usage: perl pileupRunDbsnp.pl <pileup file> <output prefix> <indels also, or only snps?  i/s>\n";

# the location of the annotations and annovar
my $annos = "/bubo/home/h18/dahlo/glob/work/apps/annovar/humandb";
my $annovar = "/bubo/home/h18/dahlo/glob/work/apps/annovar"; # why is this not in the path in perl
my $scripts = "/bubo/home/h18/dahlo/glob/work/uppmaxScripts"; # why is this not in the path in perl


# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;
my $indel = shift or die $usage;

# convert the pileup file to annovar format
$indel eq 'i' ? system("perl $scripts/conversions/pileup2annovar.pl $infile $outfile.annovar 1") : system("perl $annovar/convert2annovar.pl -format pileup $infile > $outfile.annovar");


# run annovar on it, using  gene annotations
system("perl $annovar/annotate_variation.pl -buildver hg19 -geneanno $outfile.annovar $annos");
system("perl $annovar/annotate_variation.pl -buildver hg19 -filter -dbtype snp131 $outfile.annovar $annos");
system("perl $scripts/annovar/generateStats.pl $outfile hg19 snp131");
