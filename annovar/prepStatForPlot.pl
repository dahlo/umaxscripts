#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl compareAllAnnovar.pl <in directory> <out file>
# ex.
# perl compareAllAnnovar.pl /path/to/annovarFiles/ out/

use warnings;
use strict;
use File::Path;
use Cwd;

=pod
This script will 

Pseudo code:
* 
=cut


# define messages
my $usage = "Usage: perl compareAllAnnovar.pl <in directory> <out directory>\n";

# the location of the annotations and annovar
my $scripts = "/bubo/home/h18/dahlo/glob/work/uppmaxScripts"; # why is this not in the path in perl


# get the file names
my $infolder = shift or die $usage;
my $outfile = shift or die $usage;
$infolder = absPath(remSlash($infolder));
$outfile = absPath($outfile);
mkpath(getPath($outfile));

# List all the files in the infolder
open(LS,"find $infolder -maxdepth 1 -type f |") || die "Failed: $!\n";
open OUT, ">", $outfile or die $!;  # open the result file

# get all the files
my @files;
while(<LS>){
  # remove the newline
  chomp($_);
  push(@files,$_);
}

# process the files
my @names;
my @inDb;
my @notDb;
my $tot;

# for each file
for(my $i = 0; $i <= $#files; $i++){
  ($names[$i]) =  (getName($files[$i]) =~ m/^(.+).annovar.stat$/) or die "ERROR: Invalid filename in prepStstForPlot.pl, getName($files[$i])"; # get the file name
  open FILE, "<", $files[$i] or die $!;  # open the file
  while(<FILE>){  # read each file
    if($_ =~ m/^Totalt antal:\t(\d+)/){ $tot = $1; }  # save the total number of snps
    if($_ =~ m/^Antal INTE i dbSNP:\t(\d+)/){ push(@notDb,$1); push(@inDb, $tot-$1); }  # save the number of snps NOT in dbsnp
  }
  close(FILE);  # close the file
}

# print to file
print OUT "\t".join("\t",@names)."\n";
print OUT "In dbSNP:\t".join("\t",@inDb)."\n";
print OUT "Not in dbSNP:\t".join("\t",@notDb)."\n";




### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (path/to,file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}

sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}

sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
