#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <path and prefix of annovar files> <outfile>
# ex.
# perl scriptname.pl annovarRes/myPaper outfile

use warnings;
use strict;

=pod

=cut


# define messages
my $usage = "Usage:   perl scriptname.pl <path and prefix of annovar files> <outfile>\n";

my $ens = "/proj/b2010074/HG19/ensGenes_110107.txt";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;


# suffix order
my @suffix = ('non_sym_gene_list','stop_gain_gene_list','sym_gene_list','non_sym_gene_list_not_in_dbsnp','stop_gain_gene_list_not_in_dbsnp','sym_gene_list_not_in_dbsnp');  #  names of the files them selves
my @description = ('nonsynonymous','stopgain','synonymous','nonsynonymous','stopgain','synonymous');  #  description used in the result file
my @validated = ('1','1','1','0','0','0',);  #  description used in the result file



# collect allele ratios
my %ratio;  #  the info storage, geneID = the key
open IN, "<", "$infile.annovar.exonic_variant_function" or die $!;
# for each line in the file
while(<IN>){
  #print $_;
  
  my @line = split(/\t/,$_);
  my @hits = split(/,/,$line[2]); # split it on commas signs
  
  # if there are more than 1 hit
  for(my $i = 0; $i <= $#hits; $i++){  # why should it be <= ??? (see similar comment ~30 rows down)
    
    # get the info
    $hits[$i] =~ /^(.+):(\w+):(exon\d+):(\S+)/;
    chomp($line[10]);
    
    # store the info
    if(defined $ratio{"$1$2$3$4"}){ # if this ID has been seen before
      $ratio{"$1$2$3$4"} .= ",$line[9]-$line[10]"; # add to the correct gene ID  (type:exonNumber)
      print "This is wrong, redo\n";
      
    }else{ # otherwise..
      $ratio{"$1$2$3$4"} = "$line[9]-$line[10]";  # initialize the entry
    }
  }
}




# go through the other files
my %gene;  #  the info storage, geneID = the key
for(my $j = 0; $j <= $#suffix; $j++){
  
  # open file handle
  #print "$infile.$suffix[$j]\n";
  open IN, "<", "$infile.$suffix[$j]" or die $!;
  
  # for each line in the file
  while(<IN>){
    #print $_;
    
    my @hits = split(/,/,$_); # split it on commas signs
    
    # if there are more than 1 hit
    for(my $i = 0; $i < $#hits; $i++){ # why should it NOT be <=  ??? (see similar comment ~30 rows up)
      
      # get the info
      $hits[$i] =~ /^(.+):(\w+):(exon\d+):(\S+)/;
      
      # store the info
     
      if(defined $gene{"$1$2$3$4"}){ # if this ID has been seen before
        $gene{"$1$2$3$4"} = "$2:$description[$j]:$3:$validated[$j]:".$ratio{"$1$2$3$4"}; # add to the correct gene ID  (type:exonNumber)
        #~ print "This is wrong, redo\n\n";
        #print "$1$2$3$4\n";
        
      }else{ # otherwise..
        $gene{"$1$2$3$4"} = "$2:$description[$j]:$3:$validated[$j]:".$ratio{"$1$2$3$4"};  # initialize the entry
         #print "NY\n";
      }
    }
  }
}


# go throug the genes and merge all of the same gene ID     my god.. i should really just rewrite all this mess...
my %merged;  #  the info storage, geneID = the key

# for each line in the file
foreach my $key (keys %gene){
  
  # soooooo messy...
  my @element = split(/:/,$gene{$key}); # split it on commas signs
  my $geneIdKey = shift(@element);
  my $tempInfo = join(':',@element);
  
  # store the info
  if(defined $merged{$geneIdKey}){ # if this ID has been seen before
    $merged{$geneIdKey} .= ",$tempInfo"; # add to the correct gene ID  (type:exonNumber)
    
  }else{ # otherwise..
    $merged{$geneIdKey} .= "$tempInfo"; # add to the correct gene ID  (type:exonNumber)
  }
}


# add empty entrys for all other ensemble gene ids
# for each line in the file
open ENS, "<", $ens or die $!;
<ENS>;  # throw away the header row
while(<ENS>){
  
  my @line = split(/\t/,$_);
  
  # store the info
  if(defined $merged{$line[1]}){ # if this ID has been seen before
    # do nothing
    
  }else{ # otherwise..
    $merged{$line[1]} .= "-"; # add the international Empty-sign to the row
  }
}



# print the result to a file
open OUT, ">", $outfile or die $!;

# print header
print OUT "geneId\texonicHits\n";

foreach my $key (sort keys %merged){  # for each gene ID
  # print the info
  print OUT "$key\t$merged{$key}\n";
}


# run the R script



### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (path/to,file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
