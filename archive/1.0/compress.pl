#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <directory to compress>
# ex.
# perl scriptname.pl folder/

use warnings;
use strict;
use File::Path;
use Cwd;

=pod

This script will take a directory and recursivly compress all files in it and its subdirectory's files individually.

* All original files will be deleted. *

The result will be a unchanged directory structure, with all the files in it zipped. Each
file can be decompressed seperatly if needed.

Pseudo code:

* Find all files in the specified directory, and in all subdirectories
* Calculate the md5 sum for each file
* Run tar -czf on the file and corresponding md5 sum file
* Keep only the newly created .tar.gz files

there is a bug in the program. If a user has files called XXXX and XXXX.md5. the existing XXXX.md5. file will be overwritten.
This will cause problems when verifying the md5 sums in decompression.. Only files named XXXX.md5. will be destroyed..

=cut

# define messages
my $usage = "compress2individualFilesConserveStructure.pl usage:  perl scriptname.pl <folder to compress>\n";

# get the file names
my $folder = shift or die $usage;
# convert to an absolute path
$folder = absPath($folder);

# get a list of the files in the structure
open(FILES,"find $folder -type f | ") or die "Failed: $!\n";

my $s = 0;
# process each file
for my $file (<FILES>){
  
  # get the name
  chomp($file);
  # escape the spaces in filenames
  if($file =~ /\w+\s\w+/){
    $file =~ s/\s/\\ /g;
    $s = 1;
    
  }
  my $filename = getName($file);
  
  # switch to the correct working directory
  chdir getPath($file);

  # generate a md5 sum of the files before doing anything
  system("md5sum $filename > $filename.md5.");
  
  # tar the file and the md5 sum together
  system("tar -czf $filename.tar.gz $filename $filename.md5.");
  
  # remove the intermediate files
  system("rm $filename $filename.md5.");
}





### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (path/to,file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
