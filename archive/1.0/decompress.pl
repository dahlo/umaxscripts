#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <directory to decompress>
# ex.
# perl scriptname.pl folder/

use warnings;
use strict;
use File::Path;
use Cwd;
use Digest::MD5 qw(md5 md5_hex md5_base64);


=pod

This script will reverse the effects of compress2individualFilesConserveStructure.pl
(Assumes all files in a file tree are .tar.gz files, and decompressed them, check the md5 sum, conserving the file structure)

Pseudo code:

* Find all files in the specified directory, and in all subdirectories
* Run tar -xzf on all files
* Check the md5 sum from before the compression
* Remove the md5 sum files


=cut

# define messages
my $usage = "decompress2individualFilesConserveStructure.pl usage:  perl scriptname.pl <folder to decompress>\n";

# get the file names
my $folder = shift or die $usage;
# convert to an absolute path
$folder = absPath($folder);

# get a list of the files in the structure
open(FILES,"find $folder -type f | ") or die "Failed: $!\n";




# process each file
for my $file (<FILES>){

  # get the names
  chomp($file); # remove newline
  # escape the spaces in filenames
  if($file =~ /\w+\s\w+/){
    $file =~ s/\s/\\ /g;
  }
  my $filename = getName($file);
  $filename =~ /^(.+)\.tar\.gz$/; # get the original filename
  my $orgname = $1;
  
  # switch to the correct working directory
  chdir getPath($file);

  # untar the file
  system("tar -xzf $filename");
  
  # check that the md5 sum matches
  open MD5, "<", "$orgname.md5." or die $!; # open the md5 file
  open ORG, "<", "$orgname" or die $!; # open the extracted file
  <MD5> =~ /^(.+)\s\s.+$/; # get the original md5 sum
  my $orgmd5 = $1;
  
  my $md5obj = Digest::MD5->new; # create the md5 object used to calculate the md5 sum of the file
  $md5obj->addfile(*ORG); # add the extracted file to the md5 object
  my $newmd5 = $md5obj->hexdigest; # get a md5 sum from the object
  
  # check if the md5 sum is the same
  if($orgmd5 ne $newmd5){ # if not..
    
    # print warning and keep the .tar.gz file for rescue
    print "WARNING: File $filename ($file) seems to be corrupt. The file will remain as a .tar.gz for you to examine, along with the unpacked one.\nThe rest of the files will continue to be processed.\n";
    
  # everything seems to be in order here
  }else{
    # remove the md5 and tar file
    system("rm $filename $orgname.md5.");
  }
}




### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (path/to,file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
