#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <directory to compress>
# ex.
# perl scriptname.pl folder/

use warnings;
use strict;
use File::Path;
use Cwd;

=pod

This script will take a directory and compress all files in it and its subdirectorys individually.

* All original files will be deleted. *

The result will be a unchanged directory structure, with all the files in it zipped. Each
file can be decompressed seperatly if needed.

Pseudo code:

* Find all files in the specified directory, and in all subdirectories
* Calculate the md5 sum for each file
* Run tar -czf on the file and corresponding md5 sum file
* Keep only the newly created .compr.tar.gz files

There is a bug in the program. If a user has files called XXXX and XXXX.md5. the existing XXXX.md5. file will be overwritten.
This will cause problems when verifying the md5 sums in decompression.. Only files named XXXX.md5. will be destroyed..
But then again, how many has files named something.md5.? that's just ridiculous!

=cut

# define messages
my $usage = "compress.pl usage:  perl scriptname.pl <folder to compress>\n";

# get the file names
my $folder = shift or die $usage;

# convert to an absolute path
$folder = absPath($folder);

# removing old compress.log
print "Trying to remove old compress.log\n";
system("rm $folder/compress.log");

# get a list of the files in the structure
open(FILES,"find $folder -type f | ") or die "Failed: $!\n";

# start the log file
open LOG, ">", "$folder/decompress.log" or die $!;
print LOG "Start of log:\n"; # maybe add a timestamp here?

# process each file
for my $file (<FILES>){
  
  # get the name
  chomp($file);
  # escape the spaces in filenames
  if($file =~ /\s/){
    $file =~ s/\s/\\ /g;    
  }
  
  # print file to log file (maybe add a % counter here?)
  print LOG "$file\n"; # log the file
  
  # get the filename alone
  my $filename = getName($file);
  
  # switch to the correct working directory
  chdir getPath($file);

  # generate a md5 sum of the files before doing anything
  print LOG "\tmd5sum $filename > $filename.md5."; # print command to log file
  system("md5sum $filename > $filename.md5.");
  
  # tar the file and the md5 sum together
  print LOG "\ttar -czf $filename.compr.tar.gz $filename $filename.md5."; # print command to log file
  system("tar -czf $filename.compr.tar.gz $filename $filename.md5.");
  
  # remove the intermediate files
  print LOG "\trm $filename $filename.md5."; # print command to log file
  system("rm $filename $filename.md5.");
}





### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (file.txt,path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
