#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <directory to decompress>
# ex.
# perl scriptname.pl folder/

use warnings;
use strict;

=pod

This script will reverse the effects of compress2individualFilesConserveStructure.pl
(Assumes all files in a file tree are .gz files, and decompressed them, conserving the file structure)

Pseudo code:

* Find all files in the specified directory, and in all subdirectories
* Run gunzip on each found file seperatly


=cut

# define messages
my $usage = "decompress2individualFilesConserveStructure.pl usage:  perl scriptname.pl <folder to decompress>\n";

# get the file names
my $folder = shift or die $usage;

# decompress the files
system("find $folder -type f -exec gunzip {} \\;");

