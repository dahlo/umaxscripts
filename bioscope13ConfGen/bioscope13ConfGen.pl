#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl bioscope13ConfGen.pl <run file>   (or -h for information about how to create run files!)
# ex.
# perl 

use warnings;
use strict;
use Cwd;
use Time::Local;

=pod

Pseudo code:
* Get all the arguments
* Write a submission file for SLURM using the given arguments
(* Submit the file to the queue system)
=cut


my $usage = "Usage: perl polyAlign.pl <run file>   (or -h for information about how to create run files!)\n";
my $confFileExp = "Please create your own run file with information that is needed to write a submission file to the SLURM system on the cluster.\n\nExample of a run file:\n \n";


#############################################
#######   GET VARIABLES, BEGINNING   ########
#############################################

# read the arguments
my $confFile = shift or die $usage;
if($confFile eq '-h'){ die $confFileExp; }  # check for help flag


# create a timestamp
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
$year += 1900; # to make it y2k compatible (seriously, how lame is that? :)  )
# add zero to all one-digit numbers  (9 -> 09, 10 -> 10)
$year = sprintf("%.2d", $year);
$mon = sprintf("%.2d", $mon+1); # adjust to real month, not array index
$mday = sprintf("%.2d", $mday);
$hour = sprintf("%.2d", $hour);
$min = sprintf("%.2d", $min);
$sec = sprintf("%.2d", $sec);
my $stamp = "$year-$mon-$mday-$hour.$min.$sec";
#print "$stamp\n$year\t$mon\t$mday\t$hour\t$min\t$sec\n";


# open file handles
open CONF, "<", $confFile or die "Unable to open config file: $confFile\n$!";

# read the user file
my %opt;
while (<CONF>) {
    chomp;                  # no newline
    s/#.*//;                # no comments
    s/^\s+//;               # no leading white
    s/\s+$//;               # no trailing white
    next unless length;     # anything left?
    my ($var, $value) = split(/\s*=\s*/, $_, 2); # separate with a equal sign
    $opt{$var} = $value;
}

# check that all required options are entered. How boring..
if(!(defined $opt{'proj'}) || ($opt{'proj'} eq '')){ die "$confFileExp\n\nProject ID required in config file!\n\n"; }
if(!(defined $opt{'type'}) || ($opt{'type'} eq '')){ die "$confFileExp\n\nType of data required in config file!\n\n"; }
if(!(defined $opt{'reads'}) || ($opt{'reads'} eq '')){ die "$confFileExp\n\nRead files required in config file!\n\n"; }
if(!(defined $opt{'quals'}) || ($opt{'quals'} eq '')){ die "$confFileExp\n\nQuality files required in config file!\n\n"; }
if(!(defined $opt{'ref'}) || ($opt{'ref'} eq '')){ die "$confFileExp\n\nReference genome required in config file!\n\n"; }
if(!(defined $opt{'out'}) || ($opt{'out'} eq '')){ die "$confFileExp\n\nOutput directory required in config file!\n\n"; }


# save hash values to variable names for a more beautiful code further on
my $proj = $opt{'proj'};
my $type = $opt{'type'};
my $reads = $opt{'reads'};
my $quals = $opt{'quals'};
my $ref = absPath($opt{'ref'});
my $out = absPath($opt{'out'});


#############################################
#######   GET VARIABLES, FINISHED    ########
#############################################





#############################################
#######  GENERATE FILES, BEGINNING  #########
#############################################


# if it is single end reads
if(lc($type) eq 'fragmap'){
  
  ### Generate the folder tree, if it does not exist
  system("mkdir -p $out/config");
  system("mkdir -p $out/references");
  system("mkdir -p $out/reads");
  
  # make a link to the references
  system("ln -s $ref $out/references/");
  
  # get additional settings
  if(!(defined $opt{'cmap'}) || ($opt{'cmap'} eq '')){ die "$confFileExp\n\nCMAP file required in config file!\n\n"; }
  my $f3length = getLeng($reads);
  my $cmap = absPath($opt{'cmap'});
  
  # get file names
  my $refName = getName($ref);
  
  # create links to read folders
  system("ln -s $reads $quals $out/reads");
  
  # get names
  my $readName;
  my $qualName;
  $readName = getName($reads);
  $qualName = getName($quals);

  
  ### Create config files
  
  # open file handles
  open PLAN, ">", "$out/config/wt.pe.plan" or die $!;
  open MAP, ">", "$out/config/mapping.ini" or die $!;
  open RUN1, ">", "$out/run1.sh" or die $!;
  open RUN2, ">", "$out/run2.sh" or die $!;
  
  
  
  # write wt.pe.plan
  print PLAN <<EOF;
# run the mapping
config/mapping.ini
EOF

  # write mapping.ini
  print MAP <<EOF;
pipeline.cleanup.middle.files=1
pipeline.cleanup.temp.files=1


#    Global Parameters 

run.name=Run1
sample.name=$readName
reference=$ref
output.dir=$out/output
tmp.dir=$out/tmp
intermediate.dir=$out/intermediate
log.dir=$out/log
scratch.dir=/scratch/solid

######################################
##  mapping.run
######################################
mapping.run=1
saet.dependency=1
mapping.output.dir=$out/output/F3/s_mapping
mapping.stats.output.file=$out/output/F3/s_mapping/mapping-stats.txt
read.length=$f3length
mapping.tagfiles=$out/reads/$readName
primer.set=F3
mapping.run.classic=false
matching.max.hits=100
mapping.mismatch.penalty=-2.0
mapping.qual.filter.cutoff=0
clear.zone=5
mismatch.level=4

######################################
##  small.indel.frag.run
######################################
small.indel.frag.run=1
mapping.output.dir=$out/output/F3/s_mapping
small.indel.frag.dependency=1
small.indel.frag.output.dir=$out/output/smallindelfrag
small.indel.frag.qual=$out/reads/$qualName
scratch.dir=/scratch/solid
small.indel.frag.log.dir=$out/log/smallindelfrag
small.indel.frag.intermediate.dir=$out/intermediate/smallindelfrag
cmap=$cmap
small.indel.frag.match=$out/output/F3/s_mapping/$readName.ma
small.indel.frag.indel.preset=1
small.indel.frag.error.indel=3
small.indel.frag.min.non.matched.length=10
small.indel.frag.seed.match.length=20
small.indel.frag.seed.mismatches=1
small.indel.frag.seed.repeat.limit=100
small.indel.frag.seed.location.1.75=1
small.indel.frag.seed.location.2.75=-20
small.indel.frag.seed.location.1.50=1
small.indel.frag.seed.location.2.50=-20
small.indel.frag.seed.window.size=80
cmap.skip.check=0
small.indel.frag.indel.parameters=
small.indel.frag.cmap.skip.check=0

######################################
##  ma.to.bam.run
######################################
ma.to.bam.run=1
mapping.output.dir=$out/output/F3/s_mapping
ma.to.bam.qual.file=$quals
ma.to.bam.reference=$ref
small.indel.frag.dependency=1
ma.to.bam.output.dir=$out/output/maToBam
ma.to.bam.match.file=$out/output/F3/s_mapping/$reads.ma
ma.to.bam.output.file.name=
ma.to.bam.intermediate.dir=$out/output/../intermediate/maToBam
ma.to.bam.temp.dir=$out/output/../temp/maToBam
ma.to.bam.pas.file=$out/output/smallindelfrag/indel-evidence-list.pas
ma.to.bam.output.filter=primary
ma.to.bam.gap.alignments.only=false
ma.to.bam.correct.to=reference
ma.to.bam.clear.zone=5
ma.to.bam.mismatch.penalty=-2.0
ma.to.bam.library.type=fragment
ma.to.bam.library.name=lib1
ma.to.bam.slide.name=
ma.to.bam.description=
ma.to.bam.sequencing.center=freetext
ma.to.bam.tints=agy
ma.to.bam.base.qv.max=40
ma.to.bam.temporary.dir=

######################################
##  bioscope.reports.run
######################################
bioscope.reports.run=1
ma.to.bam.output.dir=$out/output/maToBam
bfast.dependency=1
reports.output.dir=$out/output/reports
coverage.output.file.1=
histogram.output.file.1=
annotation.output.file.1=
report.input.bam.file=
histogram.file.name=coverage-histogram.txt

######################################
##  position.errors.run
######################################
position.errors.run=1
ma.to.bam.output.dir=$out/output/maToBam
bfast.dependency=1
position.errors.output.dir=$out/output/position-errors
position.errors.output.file=positionErrors.txt
show.first.position.as.base=1

EOF

  # write run1.sh
    print RUN1 <<EOF;
cd $out
sh run2.sh > frag.map-results 2> frag.map-log  &
EOF

  # write run2.sh
    print RUN2 <<EOF;
  
  
#!/bin/bash -l
source /etc/profile
module load bioinfo-tools
module load bioscope/1.3.1

# run bioscope in the background, and let it work its magic
nohup sh bioscope.sh -A $proj -l $out/log/log.log $out/config/wt.pe.plan
EOF

  # save a copy of the config file
  system("cp $confFile $out/config/$stamp.confFile");

  # launch and remove batch file
  #system("sh $out/run1.sh");
  
  #
  print "\nThe config will by default align all reads and pair them. To change this please edit the appropriate files in\n$out/config/\n\nTo start the alignment, please run the initiation file by typing:\n\ncd $out ; sh run1.sh\n\n";
  















  
}elsif(lc($type) eq 'wtpe'){
  
  # store the filter path
  my $filter = absPath($opt{'filter'});
  my $gtf = absPath($opt{'gtf'});
  
  ### Generate the folder tree, if it does not exist
  system("mkdir -p $out/config");
  system("mkdir -p $out/references");
  system("mkdir -p $out/reads/f3");
  system("mkdir -p $out/reads/f5");
  #~ mkpath("$out/intermediate");
  #~ mkpath("$out/log");
  #~ mkpath("$out/output");
  #~ mkpath("$out/temp");
  #~ mkpath("$out/tmp");
  
  # make a link to the references
  system("ln -s $ref $filter $gtf $out/references/");
  
  # get additional settings
  if(!(defined $opt{'gtf'}) || ($opt{'gtf'} eq '')){ die "$confFileExp\n\nAnnotation GTF file required in config file!\n\n"; }
  
  # get file names
  my $refName = getName($ref);
  my $filterName = getName($filter);
  my $gtfName = getName($gtf);
  
  # option stuff
  my @Reads = split(/,/,$reads);
  my @Quals = split(/,/,$quals);
  
  # make sure there is 2 of each. Die if there are NOT 2, and do nothing useful if not..
  $#Reads == 1 ? my $doNothing1=1 : die "2 and only 2 sets of reads/quals should be used with WTPE. Comma separated (in config file: reads=path/read1,/path/read2\nquals=qual1,qual2)\n";
  $#Quals == 1 ? my $doNothing2=1 : die "2 and only 2 sets of reads/quals should be used with WTPE. Comma separated (in config file: reads=path/read1,/path/read2\nquals=qual1,qual2)\n";
  
  # put them in a hash to define which is F3 and F5
  my %reads;
  my %quals;
  my %fileNames;
  my %readPaths;
  for(my $i = 0; $i <= $#Reads; $i++){
    
    if($Reads[$i] =~ m/^.*F3.*$/){ # F3
      $reads{'F3'} = absPath($Reads[$i]);
      $quals{'F3'} = absPath($Quals[$i]);
      
      # extract the last non-whitespace after the last /  ie. the bare index prefix and the path
      $reads{'F3'} =~ /(.+)\/(\S+)$/;
      $readPaths{'F3'} = $1;
      $fileNames{'F3'} = $2;
      
    }elsif($Reads[$i] =~ m/^.*F5.*$/){ # F5
      $reads{'F5'} = absPath($Reads[$i]);
      $quals{'F5'} = absPath($Quals[$i]);
      
      # extract the last non-whitespace after the last /  ie. the bare index prefix and the path
      $reads{'F5'} =~ /(.+)\/(\S+)$/;
      $readPaths{'F5'} = $1;
      $fileNames{'F5'} = $2;
      
    }else{ # neither, something is wrong!!!11
      die "ERROR: at least one of the read files name does NOT include 'F3' or 'F5'. Unable to determin which is which!\n";
    }
  }
  
  # get additional settings
  my $f3length = getLeng($reads{'F3'});
  my $f5length = getLeng($reads{'F5'});
  
  # create links to read folders
  system("ln -s $reads{'F3'} $quals{'F3'} $out/reads/f3/");
  system("ln -s $reads{'F5'} $quals{'F5'} $out/reads/f5/");
  
  # get names
  my %readNames;
  my %qualNames;
  $readNames{'F3'} = getName($reads{'F3'});
  $readNames{'F5'} = getName($reads{'F5'});
  $qualNames{'F3'} = getName($quals{'F3'});
  $qualNames{'F5'} = getName($quals{'F5'});

  
  ### Create config files
  
  # open file handles
  open PLAN, ">", "$out/config/wt.pe.plan" or die $!;
  open COM, ">", "$out/config/wt.pe.common.ini" or die $!;
  open COUNTTAG, ">", "$out/config/wt.pe.counttag.ini" or die $!;
  open F3MAP, ">", "$out/config/wt.pe.f3.mapping.ini" or die $!;
  open F3RES, ">", "$out/config/wt.pe.f3.rescue.ini" or die $!;
  open F5MAP, ">", "$out/config/wt.pe.f5.mapping.ini" or die $!;
  open F5RES, ">", "$out/config/wt.pe.f5.rescue.ini" or die $!;
  open JUNCFIND, ">", "$out/config/wt.pe.junction.finder.ini" or die $!;
  open PAIR, ">", "$out/config/wt.pe.pairing.ini" or die $!;
  open CLEANUP, ">", "$out/config/wt.pe.pipeline.cleanup.ini" or die $!;
  open VALIDATION, ">", "$out/config/wt.pe.pipeline.validation.ini" or die $!;
  open SAM2WIG, ">", "$out/config/wt.pe.sam2wig.ini" or die $!;
  open RUN1, ">", "$out/run1.sh" or die $!;
  open RUN2, ">", "$out/run2.sh" or die $!;
  
  
  
  # write wt.pe.plan
  print PLAN <<EOF;
# validate the pipeline run
#config/wt.pe.pipeline.cleanup.ini

# run f3, f5 mappings in parallel
=config/wt.pe.f3.mapping.ini
=config/wt.pe.f5.mapping.ini

# this allows f3 and f5 rescue to run in parallel after the
# mapping stages
+

# run rescue
# if rescue is turned off, change the pairing input durectories
=config/wt.pe.f3.rescue.ini
=config/wt.pe.f5.rescue.ini

# run pairing
config/wt.pe.pairing.ini

# run junction finding, counttag, and sam2wig in parallel
# run junction finding
#=config/wt.pe.junction.finder.ini
# seems to be failing alot on Kalkyl. Disabled for now, but config files will still be generated.

# run counttag
=config/wt.pe.counttag.ini

# run sam2wig
=config/wt.pe.sam2wig.ini
EOF

  # write wt.pe.common.ini
  print COM <<EOF;
######################################################################################################
#      Global settings for the pipeline run
######################################################################################################

# base directory
base.dir = ../

# output directory
output.dir = \${base.dir}/output/paired_end

# intermediate dir
intermediate.dir = \${base.dir}/intermediate

# temp dir
tmp.dir = \${base.dir}/temp

wt.developer.params = 

# genome reference
genome.reference = \${base.dir}/references/$refName

# filter reference
filter.reference = \${base.dir}/references/$filterName

# exons gtf file
gtf.file = \${base.dir}/references/$gtfName

# exon reference file
wt.f5.exseqext.output.reference = \${intermediate.dir}/reference/f5_exon_sequence_extraction/f5_exons_reference.fasta

# splice junction reference file
wt.f3.splext.output.reference = \${intermediate.dir}/reference/f3_splice_junction_extraction/f3_junctions_reference.fasta


# f3 reads and color quality file
f3.reads.file = \${base.dir}/reads/f3/$readNames{'F3'}
f3.qual.file = \${base.dir}/reads/f3/$qualNames{'F3'}

# f5 reads and color quality file
f5.reads.file = \${base.dir}/reads/f5/$readNames{'F5'}
f5.qual.file = \${base.dir}/reads/f5/$qualNames{'F5'}

# read lengths  (should maybe add to conf file?)
f3.read.length = $f3length
f5.read.length = $f5length

# make sure that required plugins are being run
#validate.pipeline = 1

# pairing output bam directory and file name
pairing.output.directory = \${output.dir}/mapping
pairing.output.bam.file = wt.pe.bam
EOF

  # write wt.pe.counttag.ini
  print COUNTTAG <<EOF;
# DIRECTIONS: run = 1 to run the plugin, run = 0 to disable the plugin.



########################################################################################
#      Global settings for the pipeline run
########################################################################################

import wt.pe.common.ini



########################################################################################
#      Local settings for the pipeline run
########################################################################################

read.length = \${f3.read.length}



#-----------------------------------------------------------------------------------------------------
#      							Count Tag Plugin
# Optional plugin.
# Parameters:	wt.counttag.input.bam.file, a .bam file
#		wt.counttag.exon.reference, an exons .gtf file.
#		wt.counttag.output.dir, output directory.
#		wt.counttag.output.file.name, output file name.
#		wt.counttag.score.clear.zone, clear zone.
#		wt.counttag.alignment.filter.mode, alignment filter mode.
#		wt.counttag.min.alignment.score, minimum alignment score.
#		wt.counttag.min.mapq, minimum map quality.
# Output:	A counttag result file in \${output.dir}/counttag.
# Description:	Counts tags from a .bam file.
#-----------------------------------------------------------------------------------------------------

wt.counttag.run = 1
wt.counttag.input.bam.file = \${pairing.output.directory}/\${pairing.output.bam.file}
wt.counttag.exon.reference = \${gtf.file}

wt.counttag.output.dir = \${output.dir}/counttag
wt.counttag.output.file.name = countagresult.txt

#wt.counttag.score.clear.zone = 5
#wt.counttag.alignment.filter.mode = primary
#wt.counttag.min.alignment.score = 0
#wt.counttag.min.mapq = 10
EOF

  # write wt.pe.f3.mapping.ini
  print F3MAP <<EOF;
# DIRECTIONS: run = 1 to run the plugin, run = 0 to disable the plugin.



########################################################################################
#      Global settings for the pipeline run
########################################################################################

import wt.pe.common.ini



########################################################################################
#      local settings for the pipeline run
########################################################################################

# read length for f3 reads
read.length = \${f3.read.length}
mapping.tagfiles = \${f3.reads.file}


#---------------------------------------------------------------------------------------
#                                F3 Genomic Mapping Plugin
#
# Required plugin.
# Parameters:	wt.genome.reference, a genomic reference file.
# Output:	A .ma file in \${intermediate.dir}/f3_genomic_map.
# Description:	Maps the reads file against the genome reference.
#---------------------------------------------------------------------------------------

wt.f3.genomic.mapping.plugin.run = 1
wt.genome.reference = \${genome.reference}



#---------------------------------------------------------------------------------------
#                                F3 Filter Mapping Plugin
# Optional plugin.
# Parameters:	wt.filter.reference, a filter reference file.
# Output:	A .ma file in \${intermediate.dir}/f3_filter_map.
# Description:	Maps the reads file against the filter reference. 
#---------------------------------------------------------------------------------------

wt.f3.filter.mapping.plugin.run = 1
wt.filter.reference = \${filter.reference}



#---------------------------------------------------------------------------------------
#                               F3 Junction Mapping Plugins
# Optional plugin.
# Parameters:	wt.genome.reference, a genome reference file.
#		wt.gtf.file, an exons .gtf file.
# Output:	A .ma file in \${intermediate.dir}/f3_junction_genomic_ma.
# Description:	Creates a splice junction reference file.
#		Maps the reads file against the junctions reference.
#		Converts the result to a .ma file with genomic coordinates.
#----------------------------------------------------------------------------------------

# run all three junction mapping plugins together, or disable all
wt.f3.splice.junction.extractor.plugin.run = 1
wt.f3.junction.mapping.plugin.run = 1
wt.f3.junction.ma.to.genomic.ma.plugin.run = 1

wt.genome.reference = \${genome.reference}
wt.gtf.file = \${gtf.file}



#---------------------------------------------------------------------------------------
#                           	F3 Ma File Merge Plugin
# Required plugin.
# Parameters:	wt.genome.reference, a genome reference.
#		wt.gtf.file. an exons .gtf file.
#		wt.filter.reference, a filter reference.
# Output:	A .ma file in \${intermediate.dir}/f3_merge
# Description:	Merges .ma files from the different f3 mappings into one .ma file.	 
#---------------------------------------------------------------------------------------

wt.f3.ma.file.merger.into.ma.file.plugin.run = 1
wt.filter.reference = \${filter.reference}
wt.genome.reference = \${genome.reference}
wt.gtf.file = \${gtf.file}
EOF

  # write wt.pe.f3.rescue.ini
  print F3RES <<EOF;
# DIRECTIONS: run = 1 to run the plugin, run = 0 to disable the plugin.



########################################################################################
#      Global settings for the pipeline run
########################################################################################

import wt.pe.common.ini



########################################################################################
#      Local settings for the pipeline run
########################################################################################



#---------------------------------------------------------------------------------------
#                           	F3 Exon Table Rescue Plugin
# Required plugin.
# Parameters:	wt.rescue.sequence.reference.file, genome reference file.
#		wt.rescue.gtf.file, exons .gtf file.
#		wt.rescue.anchor.ma.dir, anchor .ma file directory.
#		wt.rescue.target.ma.dir, target .ma file directory.
#		wt.rescue.run.input.generation, run input generation.
#		wt.rescue.run.rescue, run rescue.
#		wt.rescue.input.generation.avg.insert.size, input generation average insert size.
#		wt.rescue.input.generation.std.insert.size, input generation standard insert size.
#		wt.rescue.input.generation.rescue.only.unaligned.reads, rescue only unaligned reads.
#		wt.rescue.input.generation.rescue.short.range, rescue short range.
#		wt.rescue.input.generation.rescue.only.for.the.best.anchor.alignment, 
#		rescue only for best anchor alignment.
#		wt.rescue.input.generation.rescue.fuzzy.exon.borders. rescue fuzzy exon borders.
#		wt.rescue.input.generation.rescue.anchor.alignments.not.overlapping.exons,
#		rescue anchor alignments not overlapping exons.
#		Rescue only within rescue distance.
#		wt.rescue.input.generation.exon.fuzzy.border.width, exon border width.
#		wt.rescue.input.generation.min.alignment.distance.for.rescue, minimum alignment distance.
#		wt.rescue.mask.of.readsm mask of reads.
#		wt.rescue.max.mismatches.allowed, maximum number of allowed mismatches.
#		wt.rescue.target.read.length, target read length.
# Output:	A .ma file in \${intermediate.dir}/f3_rescue
# Description:	Uses the anchor to rescue reads from the target.
#---------------------------------------------------------------------------------------

wt.f3.exon.table.rescue.plugin.run = 1

wt.rescue.sequence.reference.file = \${genome.reference}
wt.rescue.gtf.file = \${gtf.file}

wt.rescue.output.dir = \${intermediate.dir}/f3_rescue
wt.rescue.target.read.length = \${f3.read.length}
wt.rescue.anchor.ma.dir = \${intermediate.dir}/f5_merge
wt.rescue.target.ma.dir = \${intermediate.dir}/f3_merge

#wt.rescue.run.input.generation = 1
#wt.rescue.run.rescue = 1

#wt.rescue.input.generation.avg.insert.size = 120
#wt.rescue.input.generation.std.insert.size = 60

#wt.rescue.input.generation.rescue.only.unaligned.reads = 0
#wt.rescue.input.generation.rescue.short.range = 1
#wt.rescue.input.generation.rescue.only.for.the.best.anchor.alignment = 0
#wt.rescue.input.generation.rescue.fuzzy.exon.borders = 1
#wt.rescue.input.generation.rescue.anchor.alignments.not.overlapping.exons = 1

#wt.rescue.input.generation.rescue.only.within.rescue.distance = 1
#wt.rescue.input.generation.exon.fuzzy.border.width = 10
#wt.rescue.input.generation.min.alignment.distance.for.rescue = 100000
#wt.rescue.mask.of.reads = 11111111111111111111111111111111111111111111100000

#wt.rescue.max.mismatches.allowed = 8



#---------------------------------------------------------------------------------------
#                           	F3 Rescue Ma File Merge Plugin
# Required plugin.
# Parameters:	wt.genome.reference, genome reference file.
#		wt.filter.reference, filter reference file.
#		wt.gtf.file, exons .gtf file.
# Output:	A .ma file in \${intermediate.dir}/f3_rescue_merge.
# Description:	Merges f3 merged .ma and f3 rescue ma and (optionally f3 filter ma)
#---------------------------------------------------------------------------------------

wt.f3.rescue.ma.file.merger.into.ma.file.plugin.run = 1

wt.genome.reference = \${genome.reference}
wt.filter.reference = \${filter.reference}
wt.gtf.file = \${gtf.file}
EOF

  # write wt.pe.f5.mapping.ini
  print F5MAP <<EOF;
# DIRECTIONS: run = 1 to run the plugin, run = 0 to disable the plugin.



########################################################################################
#      Global settings for the pipeline run
########################################################################################

import wt.pe.common.ini



########################################################################################
#      Local settings for the pipeline run
########################################################################################

# read length for f5 reads
read.length = \${f5.read.length}
mapping.tagfiles = \${f5.reads.file}



#---------------------------------------------------------------------------------------
#                                F5 Genomic Mapping Plugin
# Required plugin.
# Parameters:	wt.genome.referfence, a genomic reference file.
# Output:	A .ma file in \${intermediate.dir}/f5_genomic_map.
# Description:	Maps the reads file against the genome reference. 
#---------------------------------------------------------------------------------------

wt.f5.genomic.mapping.plugin.run = 1
wt.genome.reference = \${genome.reference}



#---------------------------------------------------------------------------------------
#                                F5 Filter Mapping Plugin
# Optional plugin. 
# Parameters:	wt.filter.reference, a filter reference file.
# Output:	A .ma file in \${intermediate.dir}/f5_filter_map.
# Description:	Maps the reads file against the filter reference.
#---------------------------------------------------------------------------------------

wt.f5.filter.mapping.plugin.run = 1
wt.filter.reference = \${filter.reference}



#---------------------------------------------------------------------------------------
#                              F5 Exon Mapping Plugins
# Optional plugin.
# Parameters:	wt.genome.reference, a genome reference.
#		wt.gtf.file, an exons .gtf file.
# Output:	A .ma file in \${intermediate.dir}/f5_exon_genomic_ma
# Description:	Creates an exon reference file.
#		Maps the reads file against the exon reference.
#		Converts the result to a .ma file with genomic coordinates.
#---------------------------------------------------------------------------------------

# run all three exon mapping plugins together, or disable them all
wt.f5.exon.sequence.extractor.plugin.run = 1
wt.f5.exon.mapping.plugin.run = 1
wt.f5.exon.ma.to.genomic.ma.plugin.run = 1

wt.genome.reference = \${genome.reference}
wt.gtf.file = \${gtf.file}



#---------------------------------------------------------------------------------------
#                           	F5 Ma File Merge Plugin
# Required plugin.
# Parameters:	wt.filter.reference, a Genome reference.
#		wt.gtf.file, an Exons .gtf file.
#		wt.filter.reference, a Filter reference.
# Output:	A .ma file in \${intermediate.dir}/f5_merge
# Description:	Merges .ma files from the different f5 mappings into one .ma file.	 
#---------------------------------------------------------------------------------------

wt.f5.ma.file.merger.into.ma.file.plugin.run = 1
wt.filter.reference = \${filter.reference}
wt.genome.reference = \${genome.reference}
wt.gtf.file = \${gtf.file}
EOF

  # write wt.pe.f5.rescue.ini
  print F5RES <<EOF;
# DIRECTIONS: run = 1 to run the plugin, run = 0 to disable the plugin.



########################################################################################
#      Global settings for the pipeline run
########################################################################################

import wt.pe.common.ini



########################################################################################
#      Local settings for the pipeline run
########################################################################################



#---------------------------------------------------------------------------------------
#                           	F5 Exon Table Rescue Plugin
# Required plugin.
# Parameters:	wt.rescue.sequence.reference.file, genome reference file.
#		wt.rescue.gtf.file, exons .gtf file.
#		wt.rescue.anchor.ma.dir, anchor .ma file directory.
#		wt.rescue.target.ma.dir, target .ma file directory.
#		wt.rescue.run.input.generation, run input generation.
#		wt.rescue.run.rescue, run rescue.
#		wt.rescue.input.generation.avg.insert.size, input generation average insert size.
#		wt.rescue.input.generation.std.insert.size, input generation standard insert size.
#		wt.rescue.input.generation.rescue.only.unaligned.reads, rescue only unaligned reads.
#		wt.rescue.input.generation.rescue.short.range, rescue short range.
#		wt.rescue.input.generation.rescue.only.for.the.best.anchor.alignment, 
#		rescue only for best anchor alignment.
#		wt.rescue.input.generation.rescue.fuzzy.exon.borders. rescue fuzzy exon borders.
#		wt.rescue.input.generation.rescue.anchor.alignments.not.overlapping.exons,
#		rescue anchor alignments not overlapping exons.
#		Rescue only within rescue distance.
#		wt.rescue.input.generation.exon.fuzzy.border.width, exon border width.
#		wt.rescue.input.generation.min.alignment.distance.for.rescue, minimum alignment distance.
#		wt.rescue.mask.of.readsm mask of reads.
#		wt.rescue.max.mismatches.allowed, maximum number of allowed mismatches.
#		wt.rescue.target.read.length, target read length.
# Output:	A .ma file in \${intermediate.dir}/f5_rescue
# Description:	Uses the anchor to rescue reads from the target.
#---------------------------------------------------------------------------------------

wt.f5.exon.table.rescue.plugin.run = 1

wt.rescue.sequence.reference.file = \${genome.reference}
wt.rescue.gtf.file = \${gtf.file}

wt.rescue.output.dir = \${intermediate.dir}/f5_rescue
wt.rescue.target.read.length = \${f5.read.length}
wt.rescue.anchor.ma.dir = \${intermediate.dir}/f3_merge
wt.rescue.target.ma.dir = \${intermediate.dir}/f5_merge

#wt.rescue.run.rescue = 1
#wt.rescue.run.input.generation = 1

#wt.rescue.input.generation.avg.insert.size = 120
#wt.rescue.input.generation.std.insert.size = 60

#wt.rescue.input.generation.rescue.only.unaligned.reads = 0
#wt.rescue.input.generation.rescue.short.range = 1
#wt.rescue.input.generation.rescue.only.for.the.best.anchor.alignment = 0
#wt.rescue.input.generation.rescue.fuzzy.exon.borders = 1
#wt.rescue.input.generation.rescue.anchor.alignments.not.overlapping.exons = 1

#wt.rescue.input.generation.rescue.only.within.rescue.distance = 1
#wt.rescue.input.generation.exon.fuzzy.border.width = 10
#wt.rescue.input.generation.min.alignment.distance.for.rescue = 100000

#wt.rescue.max.mismatches.allowed = 6



#---------------------------------------------------------------------------------------
#                           	F5 Rescue Ma File Merge Plugin
# Required plugin.
# Parameters:	wt.genome.reference, genome reference file.
#		wt.filter.reference, filter reference file.
#		wt.gtf.file, exons .gtf file.
# Output:	A .ma file in \${intermediate.dir}/f5_rescue_merge.
# Description:	Merges f5 merged .ma and f5 rescue ma and (optionally f5 filter ma)
#---------------------------------------------------------------------------------------

wt.f5.rescue.ma.file.merger.into.ma.file.plugin.run = 1

wt.genome.reference = \${genome.reference}
wt.filter.reference = \${filter.reference}
wt.gtf.file = \${gtf.file}
EOF

  # write wt.pe.junction.finder.ini
  print JUNCFIND <<EOF;
# DIRECTIONS: run = 1 to run the plugin, run = 0 to disable the plugin.



########################################################################################
#      Global settings
########################################################################################

import wt.pe.common.ini



########################################################################################
#      Local settings
########################################################################################

#---------------------------------------------------------------------------------------
#                               Junction Finder Plugins
#
# Required plugin.
# Parameters:	wt.junction.finder.input.bam, a .bam file.
#		wt.gtf.file, an exon .gtf file.
#		wt.junction.finder.input.exon.reference, an exon reference.			
#		wt.genome.reference, a genome reference.
#		wt.junction.finder.min.exon.length, minimum exon length.
#		wt.junction.finder.first.read.max.read.length, first read length.
#		wt.junction.finder.first.read.max.read.length, Second read length.
#		wt.junction.finder.single.read, run single read fusion finding.
#		wt.junction.finder.single.read.min.mapq, minimum mapping quality for single reads.
#		wt.junction.finder.single.read.min.overlap, single read minimum overlap.
#		wt.junction.finder.single.read.max.mismatches, single read maximum mismatches.
#		wt.junction.finder.single.read.clip.size, single read clip size at the end of the read.
#		wt.junction.finder.single.read.clip.total, single read total size for clipping.
#		wt.junction.finder.single.read.ReportMultihit, single read report multiple hits.
#		wt.junction.finder.single.read.remap, single read remap.
#		wt.junction.finder.single.read.clip.5.prime, single read clip the end of the 3' and 5' end of the read.
#		wt.junction.finder.single.read.min.read.length, single read minimum read length considered.
#		wt.junction.finder.paired.read, paired read run.
#		wt.junction.finder.paired.read.min.mapq, paired read minimum map quality.
#		wt.junction.finder.paired.read.avg.insert.size, paired read average insert size.
#		wt.junction.finder.paired.read.std.insert.size, paired read standard insert size.
#		wt.junction.finder.single.read.min.evidence.for.junctionm single read minimum evidence for junction.
#		wt.junction.finder.paired.read.min.evidence.for.junction, paired read minimum evidence for junction.
#		wt.junction.finder.combined.min.evidence.for.junction, combined minimum evidence for junction.
#		wt.junction.finder.single.read.min.evidence.for.alt.splice, single read minimum evidence for alternative splices.
#		wt.junction.finder.paired.read.min.evidence.for.alt.splice, paired read minimum evidence for alternative splices.
#		wt.junction.finder.combined.min.evidence.for.alt.splice, combined minimum evidence for alternative splices.
#		wt.junction.finder.single.read.min.evidence.for.fusion, single read minimum evidence for fusions.
#		wt.junction.finder.paired.read.min.evidence.for.fusion, paired read minimum evidence for fusions.
#		wt.junction.finder.combined.evidence.for.fusion, combined minimum evidence for fusions.
#		wt.junction.finder.show.same.exon.pairs, show same exon pairs.
#		wt.junction.finder.output.format, output format.
# Output:	Files containing junctions, fusions, and alternative splices
# 		in \${output.dir}/paired_end/junction_finder.
# Description:	Finds junctions, fusions, and alternative splices.
#---------------------------------------------------------------------------------------

wt.f5.exon.sequence.extractor.plugin.run = 1
wt.f3.f5.junction.finder.plugin.run = 1

wt.genome.reference = \${genome.reference}
wt.gtf.file = \${gtf.file}

wt.junction.finder.gtf.file = \${wt.gtf.file}
wt.junction.finder.input.exon.reference = \${wt.f5.exseqext.output.reference}
wt.junction.finder.input.bam = \${pairing.output.directory}/\${pairing.output.bam.file}
wt.junction.finder.output.dir = \${output.dir}/junction_finder

wt.junction.finder.min.exon.length = 25
wt.junction.finder.first.read.max.read.length = 50
wt.junction.finder.second.read.max.read.length = 25

#wt.junction.finder.single.read = 1
#wt.junction.finder.single.read.min.mapq = 0
#wt.junction.finder.single.read.min.overlap = 10
#wt.junction.finder.single.read.max.mismatches = 2
#wt.junction.finder.single.read.clip.size = 2
#wt.junction.finder.single.read.clip.total = 10
#wt.junction.finder.single.read.ReportMultihit = 0 
#wt.junction.finder.single.read.remap = 0
#wt.junction.finder.single.read.clip.5.prime = 1
#wt.junction.finder.single.read.min.read.length = 37

#wt.junction.finder.paired.read = 1
#wt.junction.finder.paired.read.min.mapq = 10
#wt.junction.finder.paired.read.avg.insert.size = 120
#wt.junction.finder.paired.read.std.insert.size = 60

#wt.junction.finder.single.read.min.evidence.for.junction = 1
#wt.junction.finder.paired.read.min.evidence.for.junction = 1
#wt.junction.finder.combined.min.evidence.for.junction = 2

#wt.junction.finder.single.read.min.evidence.for.alt.splice = 1
#wt.junction.finder.paired.read.min.evidence.for.alt.splice = 1
#wt.junction.finder.combined.min.evidence.for.alt.splice = 3

# For single barcode analysis, use 1,1,2, respectively, for the three parameters below
#wt.junction.finder.single.read.min.evidence.for.fusion = 2
#wt.junction.finder.paired.read.min.evidence.for.fusion = 2
#wt.junction.finder.combined.evidence.for.fusion = 4

#wt.junction.finder.show.same.exon.pairs = 0

# To generate a BED file, use 2, to generate .SEQ files, use 3
#wt.junction.finder.output.format = 1
EOF

  # write wt.pe.pairing.ini
  print PAIR <<EOF;
# DIRECTIONS: run = 1 to run the plugin, run = 0 to disable the plugin.



########################################################################################
#      Global settings for the pipeline run
########################################################################################

import wt.pe.common.ini



########################################################################################
#      Local settings for the pipeline run
########################################################################################



#---------------------------------------------------------------------------------------
#				Pairing Plugin 
#
# Required plugin.
# Parameters:	insert.start, Insert start.
#		insert.end, Insert end.
#		pairing.output.dir, pairing output directory.
#		bam.file.name, output bam file name.
#		mate.pairs.tagfile.dirs, input directories containing .ma files.
#		mate.pairs.rescue.level, rescue level (off by default)
# Output:	wt.pe.bam in \${pairing.output.directory}/\${pairing.output.bam.file}
# Description:	Matches pairs of reads from F3 and F5 mapping results.
#---------------------------------------------------------------------------------------

# run pairing plugin
pairing.wt.run = 1

# color quality files
pairing.color.qual.file.path.1 = \${f3.qual.file}
pairing.color.qual.file.path.2 = \${f5.qual.file}

# genome reference
reference = \${genome.reference}

pairing.output.dir = \${pairing.output.directory}
bam.file.name = \${pairing.output.bam.file}

# mate pair insert start and end
insert.start = 30
insert.end = 100000

# the following setting assumes pairing without pairing rescue. the pairing with pairing
# rescue parameters in the next paragraph must be disabled, and the rescue stages must be
# enabled in the plan file when enabling this option.
mate.pairs.tagfile.dirs = \${intermediate.dir}/f3_rescue_merge,\${intermediate.dir}/f5_rescue_merge

# the following two parameters enable pairing with pairing in rescue.
# disable f3 and f5 rescue in the bioscope plan when enabling this option.
#mate.pairs.tagfile.dirs = \${intermediate.dir}/f3_merge,\${intermediate.dir}/f5_merge
#mate.pairs.rescue.level = 4
EOF

  # write wt.pe.pipeline.cleanup.ini
  print CLEANUP <<EOF;
######################################################################################################
#      Global settings for the pipeline run
######################################################################################################

import wt.pe.common.ini



######################################################################################################
#      Local settings for the pipeline run
######################################################################################################

#-----------------------------------------------------------------------------------------------------
#                               Pipeline Cleanup Plugin
# Optional plugin.  
# Description:		Deletes any existing files in the output directory for this run.
#
#-----------------------------------------------------------------------------------------------------

wt.f3.f5.pipeline.cleanup.plugin.run = 1
EOF

  # write wt.pe.pipeline.validation.ini
  print VALIDATION <<EOF;
########################################################################################
#      local settings for the pipeline run
########################################################################################

import wt.pe.common.ini

wt.f3.f5.pipeline.validation.plugin.run = 1

#import wt.pe.f3.mapping.ini
#import wt.pe.f5.mapping.ini
#import wt.pe.f5.rescue.ini
#import wt.pe.pairing.ini
#import wt.pe.junction.finder.ini
#import wt.pe.counttag.ini
#import wt.pe.sam2wig.ini
EOF

  # write wt.pe.sam2wig.ini
  print SAM2WIG <<EOF;
# DIRECTIONS: run = 1 to run the plugin, run = 0 to disable the plugin.



########################################################################################
#      Global settings for the pipeline run
########################################################################################

import wt.pe.common.ini



########################################################################################
#      Local settings for the pipeline run
########################################################################################

read.length = \${f3.read.length}



#-----------------------------------------------------------------------------------------------------
#      							Sam2wig Plugin
# Optional plugin.
# Parameters:	wt.sam2wig.input.bam.file, a .bam file
#		wt.sam2wig.output.dir, output directory.
#		wt.sam2wig.basefilename, base file name.
#		wt.sam2wig.alignment.score, minimum alignment score.
#		wt.sam2wig.min.coverage, minimum coverage.
#		wt.sam2wig.wigperchromosome, wig per chromosome.
#		wt.sam2wig.alignment.filter.mode, filter mode.
#		wt.sam2wig.score.clear.zone, clear zone.
#		wt.sam2wig.min.mapq, minimum mapping quality.
# Output:	a .wig file in \${output.dir}/sam2wig.
# Description:	Generates a coverage .wig file from a .bam file.
#-----------------------------------------------------------------------------------------------------

wt.sam2wig.run = 1

wt.sam2wig.input.bam.file = \${pairing.output.directory}/\${pairing.output.bam.file}

wt.sam2wig.output.dir = \${output.dir}/sam2wig
wt.sam2wig.basefilename = coverage

#wt.sam2wig.alignment.score = 0
#wt.sam2wig.min.coverage = 10
#wt.sam2wig.wigperchromosome = true
#wt.sam2wig.alignment.filter.mode = primary
#wt.sam2wig.score.clear.zone = 5
#wt.sam2wig.min.mapq = 10
EOF

  # write run1.sh
    print RUN1 <<EOF;
cd $out
sh run2.sh > wt.pe-results 2> wt.pe-log  &
EOF

  # write run2.sh
    print RUN2 <<EOF;
  
  
#!/bin/bash -l
source /etc/profile
module load bioinfo-tools
module load bioscope/1.3.1

# run bioscope in the background, and let it work its magic
nohup sh bioscope.sh -A $proj -l $out/log/log.log $out/config/wt.pe.plan
EOF

  # save a copy of the config file
  system("cp $confFile $out/config/$stamp.confFile");

  # launch and remove batch file
  #system("sh $out/run1.sh");
  
  #
  print "\nThe config will by default align all reads and pair them. To change this please edit the appropriate files in\n$out/config/\n\nTo start the alignment, please run the initiation file by typing:\n\ncd $out ; sh run1.sh\n\n";
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}elsif(lc($type) eq 'dnamate'){
  
  ### Generate the folder tree, if it does not exist
  system("mkdir -p $out/config");
  system("mkdir -p $out/references");
  system("mkdir -p $out/F3/reads1");
  system("mkdir -p $out/R3/reads2");
  #~ mkpath("$out/intermediate");
  #~ mkpath("$out/log");
  #~ mkpath("$out/output");
  #~ mkpath("$out/temp");
  #~ mkpath("$out/tmp");
  
  # make a link to the references
  system("ln -s $ref $out/references/");
  
  # get file names
  my $refName = getName($ref);
  
  # option stuff
  my @Reads = split(/,/,$reads);
  my @Quals = split(/,/,$quals);
  
  # make sure there is 2 of each. Die if there are NOT 2, and do nothing useful if not..
  $#Reads == 1 ? my $doNothing1=1 : die "2 and only 2 sets of reads/quals should be used with dnamate. Comma separated (in config file: reads=path/read1,/path/read2\nquals=qual1,qual2)\n";
  $#Quals == 1 ? my $doNothing2=1 : die "2 and only 2 sets of reads/quals should be used with dnamate. Comma separated (in config file: reads=path/read1,/path/read2\nquals=qual1,qual2)\n";
  
  # put them in a hash to define which is F3 and R3
  my %reads;
  my %quals;
  my %fileNames;
  my %readPaths;
  for(my $i = 0; $i <= $#Reads; $i++){
    
    if($Reads[$i] =~ m/^.*F3.*$/){ # F3
      $reads{'F3'} = absPath($Reads[$i]);
      $quals{'F3'} = absPath($Quals[$i]);
      
      # extract the last non-whitespace after the last /  ie. the bare index prefix and the path
      $reads{'F3'} =~ /(.+)\/(\S+)$/;
      $readPaths{'F3'} = $1;
      $fileNames{'F3'} = $2;
      
    }elsif($Reads[$i] =~ m/^.*R3.*$/){ # R3
      $reads{'R3'} = absPath($Reads[$i]);
      $quals{'R3'} = absPath($Quals[$i]);
      
      # extract the last non-whitespace after the last /  ie. the bare index prefix and the path
      $reads{'R3'} =~ /(.+)\/(\S+)$/;
      $readPaths{'R3'} = $1;
      $fileNames{'R3'} = $2;
      
    }else{ # neither, something is wrong!!!11
      die "ERROR: at least one of the read files name does NOT include 'F3' or 'F5'. Unable to determin which is which!\n";
    }
  }
  
  # get additional settings
  my $f3length = getLeng($reads{'F3'});
  my $r3length = getLeng($reads{'R3'});
  print "$f3length\t$r3length\n";
  
  # create links to read folders
  system("ln -s $reads{'F3'} $quals{'F3'} $out/F3/reads1/");
  system("ln -s $reads{'R3'} $quals{'R3'} $out/R3/reads2/");
  
  # get names
  my %readNames;
  my %qualNames;
  $readNames{'F3'} = getName($reads{'F3'});
  $readNames{'R3'} = getName($reads{'R3'});
  $qualNames{'F3'} = getName($quals{'F3'});
  $qualNames{'R3'} = getName($quals{'R3'});

  
  ### Create config files
  
  # open file handles
  open PLAN, ">", "$out/analysis.plan" or die $!;
  open F3, ">", "$out/F3/F3.ini" or die $!;
  open GLOBAL, ">", "$out/global.ini" or die $!;
  open R3, ">", "$out/R3/R3.ini" or die $!;
  open MP, ">", "$out/pairing.ini" or die $!;
  open RUN1, ">", "$out/run1.sh" or die $!;
  open RUN2, ">", "$out/run2.sh" or die $!;
  
  
  
  # analysis.plan
  print PLAN <<EOF;
=./F3/F3.ini
=./R3/R3.ini
./pairing.ini
EOF


  # write global.ini
  print GLOBAL <<EOF;
############################
############################
##
##  global parameters
##

# Working directory
base.dir=./
# Output directory
output.dir = \${base.dir}/outputs
# Directory for writing temporary files
temp.dir = \${base.dir}/temp
# Directory for writing intermediate results
intermediate.dir = \${base.dir}/intermediate
# Log directory
log.dir = \${base.dir}/log
# If application need fasta/qual file, this variable should point to the folder where those are.
reads.result.dir.1 = \${base.dir}/../F3/reads1
# Second fasta/qual location
reads.result.dir.2 = \${base.dir}/../R3/reads2
# Absolute folder location of reference
reference.dir = $out/references/
# scratch directory
scratch.dir=/scratch/solid

# override it locally in the pipeline ini file when needed
primer.set = F3
EOF


  # write f3.ini
  print F3 <<EOF;
  





##################################
##################################
##
##  global settings for the pipeline run
##
import ../global.ini
reference = \${reference.dir}/$refName
read.length = $f3length


##################################
##################################
##
##  mapping pipeline
##
mapping.run = 1
mapping.tagfiles.dir =\${base.dir}/reads1
mapping.output.dir = \${output.dir}/s_mapping
mapping.run.classic=false
matching.max.hits=100
mapping.mismatch.penalty=-2.0
mapping.qual.filter.cutoff=0
clear.zone=5
mismatch.level=6


##################################
##################################
##
##  temp files and folders keep 
##  # don't keep for clean run ...   make it =1, if you want them not to be deleted.
#pipeline.cleanup.middle.files = 0
#job.cleanup.temp.files = 0



EOF

  # write r3.ini
  print R3 <<EOF;
  



##################################
##################################
##
##  global settings for the pipeline run
##
import ../global.ini
reference = \${reference.dir}/$refName
read.length = $r3length
reads.result.dir.1 = \${base.dir}/reads2




##################################
##################################
##
##  mapping pipeline
##
mapping.run = 1
mapping.tagfiles.dir = \${base.dir}/reads2
mapping.output.dir = \${output.dir}/s_mapping
mapping.run.classic=false
matching.max.hits=100
mapping.mismatch.penalty=-2.0
mapping.qual.filter.cutoff=0
clear.zone=5
mismatch.level=6


##################################
##################################
##
##  temp files and folders keep 
##
#pipeline.cleanup.middle.files = 0
#job.cleanup.temp.files = 0
EOF

  # write pairing.ini
  print MP <<EOF;
#           To include some common variables.
import global.ini
#Reference genome file name.
reference = \${reference.dir}/$refName
reads.result.dir.1 = \${base.dir}/F3/reads1
reads.result.dir.2 = \${base.dir}/R3/reads2


## *************************************************************
##   pairing
## ************************************************************

# mandatory parameters
# --------------------
# Parameter specifies whether to run or not pairing pipeline. [1: to run, 0:to not run]
pairing.run = 1
# Mapping output directories 
mate.pairs.tagfile.dirs = \${base.dir}/F3/outputs/s_mapping,\${base.dir}/R3/outputs/s_mapping

pairing.output.dir = \${output.dir}/pairing

# optional parameters
# -------------------

######################################
##  bioscope.reports.run
######################################
bioscope.reports.run=1
pairing.output.dir=\${output.dir}/pairing
bfast.dependency=1
reports.output.dir=\${output.dir}/reports
coverage.output.file.1=
coverage.output.file.2=
histogram.output.file.1=
histogram.output.file.2=
annotation.output.file.1=
annotation.output.file.2=
report.input.bam.file=
histogram.file.name=coverage-histogram.txt

######################################
##  position.errors.run
######################################
position.errors.run=1
pairing.output.dir=\${output.dir}/pairing
bfast.dependency=1
position.errors.output.dir=\${output.dir}/position-errors
position.errors.output.file=positionErrors.txt
show.first.position.as.base=1

# Selects a set of parameters for indel search: 1: Deletions to 11, insertions to 3, Small indels. 2: Deletions to 14, insertions to 4, 
# Small indels. 3: Insertions from 4 to 14 4: Insertions from 15 to 20. 5: Longer deletions from 12 to 500. 
# Any of the values 1-5 may be entered, separated by comments
#indel.preset.parameters = 1,3,4,5

# Max Base QV. - The maximum value for a base quality value
#max.base.qv = 40

# Minimum Insert - Minimum insert size defining a good mate. If this is not set the code will attempt to measure the best value
#insert.start = 

# Maximum Insert - Maximum insert size defining a good mate. If this is not set the code will attempt to measure the best value
#insert.end = 

# Rescue Level - "Usually 2 * the mismatch level
#mate.pairs.rescue.level = 4

# Pairing statistics file name 
#mates.stats.report.name = pairingStats.stats

# Max Hits for Indel Search
#indel.max.hits = 10

# Maximum Hits
#matching.max.hits = 100

# Mapping Mismatch Penalty
#mapping.mismatch.penalty = -2.0

# Parameter specifies the alignment size of the anchor region
#pairing.anchor.length=25

# Minimum Non-mapped Length for Indels
#indel.min.non-matched.length = 10

# Rescue Level for Indels - Default for 50mers,3 for 35mers, and 2 for 25mers.
#indel.max.mismatches = 5

# Use template Rescue File For Indels
#use.template.rescue.file = true

# Max mismatches in indel search for tag 1
#pairing.indel.max.mismatch.tag1 = 5

# Max mismatches in indel search for tag 2
#pairing.indel.max.mismatch.tag2 = 5

# Pair Uniqueness Threshold
#pair.uniqueness.threshold = 10.0

# Maximum estimated insert size
#max.insert.estimate = 20000

# Minimum estimated insert size
#min.insert.estimate = 0

# Primer set - Use this only when both directories specified by mate.pairs.tagfile.dirs are the same. Then the files must have these strings 
# immediately before the .csfasta, if present, or the .ma extension.
# primer.set = F3,R3

# Mark PCR and optical duplicates
#pairing.mark.duplicates = true

# Color quality file path 1 - Color quality file path for first tag. Use instead of reads directories.
#pairing.color.qual.file.path.1 = 

# Color quality file path 2 - If either file path is explicitly set, both must be set
#pairing.color.qual.file.path.2 = 

# Annotations: How to correct color calls - 
#Specifies how to correct the color calls.
# 'missing' - Replaces all inconsistent read-colors with '.'. These will translate to 'x' in the base space representation, attribute 'b'.
# 'reference' - Replaces all read-colors annotated inconsistent (i.e., 'a' or 'b') with the corresponding reference color.
# 'singles' - Replaces all 'single' inconsistent colors (i.e., those annotated 'a' or 'b' and not adjacent to another 'b') with the corresponding 
#	reference color. Replaces all other inconsistent colors with '.'.
# 'consistent' - For each block of contiguous inconsistent colors, replace all single insistent colors 
#	(i.e., those annotated 'a' or 'b' and not adjacent to another 'b') with the corresponding reference color. Replace all other inconsistent 
# colors with '.'.
# 'qvThreshold' - A scheme combining the four above choices, based on the specified qvThreshold. (--correctTo: default is missing)
#pairing.correct.to = reference

# Single-tint annotation - Represents any number of single-tint annotations.
# 'a' - Isolated single-color mismatches (grAy).
# 'g' - Color position that is consistent with an isolated one-base variant (e.g., SNP).
# 'y' - Color position that is consistent with an isolated two-base variant.
#    (default is agy if not specified.)
#pairing.tints = agy

# User Library prefix - Prefix for LB attribute of BAM file. Accepts any characters except tab and hyphen
#pairing.library.name =

# Parameter specifies the path to the ma result file of F3 mapping
#pairing.first.mapping.file=

# Parameter specifies the path to the ma result file of R3 mapping
#pairing.second.mapping.file=

# Parameter specifies filter for the records in the output  bam file ['primary' specifies only primary alignments, 'none']
# Default value when not specified is 'primary'
#pairing.output.filter=primary


##################################
##
##  temp files and folders keep
##  # don't keep temp files and folders for clean run ...   make it =1, if you want them to be deleted.
#job.cleanup.temp.files = 0

EOF


  # write run1.sh
    print RUN1 <<EOF;
cd $out
sh run2.sh > dnamate-results 2> dnamate-log  &
EOF

  # write run2.sh
    print RUN2 <<EOF;
  
  
#!/bin/bash -l
source /etc/profile
module load bioinfo-tools
module load bioscope/1.3.1

# run bioscope in the background, and let it work its magic
nohup sh bioscope.sh -A $proj -l $out/log/log.log $out/analysis.plan
EOF

  # save a copy of the config file
  system("cp $confFile $out/config/$stamp.confFile");

  # launch the batch file
  #system("sh $out/run1.sh");
  
  #
  print "\nThe config will by default align all reads and pair them. To change this please edit the appropriate files in\n$out/config/\n\nTo start the alignment, please run the initiation file by typing:\n\ncd $out ; sh run1.sh\n\n";
  














  
}

#############################################
#######  GENERATE FILES, FINISHED   #########
#############################################



### Methods

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (file.txt,path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element (the file name)
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Removes the / sign, if any, at the beginning of directory names
sub remFirstSlash{
  my $str = shift;
  my @arr;
  if($str =~ m/^\//){
    @arr = split(//,$str);
    shift(@arr);
    $str = join('',@arr);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}


# get the length of the 11th read of a csfasta file
sub getLeng{
  
  # get the file name
  my $f = shift;
  
  # the the 10th row
  open(LINE, "sed '11q;d' $f | ");
  my $l = <LINE>;
  
  # return the length, minus 2 for the primer base and perl array index shift
  return (length($l)-2);
  
}
