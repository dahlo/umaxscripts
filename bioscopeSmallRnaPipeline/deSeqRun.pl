#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <outfile> <infile1> (<infile2> <infile3> ... <infileN>)
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "Usage:  perl scriptname.pl <outfile> <infile1> (<infile2> <infile3> ... <infileN>)\n";

# get the file names
my $outfile = shift or die $usage;
my @infiles = @ARGV or die $usage;


# summarize all the infiles

my %collection;
my $fileNr = 0;
# for each infile
foreach my $file (@infiles){
  
  # open file handle
  open IN, "<", $file or die $!;
  
  # for each line in the file
  while(<IN>){
    #~ print "$i\n";
    
    my @line = split(/\t/, $_); # split the current line on tabs
    my $key = $line[8]; # get the key row
    $key =~ m/ACC="(.+)"; ID="(.+)";/;  # get the two different IDs
    $key = $1;  # keep one of them for future identification (easy to switch which, if need arises)
    
    
    if(defined $collection{$key}){ # if this ID has been seen before
      $collection{$key}[$fileNr] += $line[7]; # add to the occurrence counter
      
    }else{ # otherwise..
      @{ $collection{$key} } = ((0) x ($#infiles + 1));  # initialize the entry with zeros for each file
      $collection{$key}[$fileNr] = $line[7];  # add the count to the correct field
      
    }
  }
  $fileNr++;  # add the file number
}



# print the result to a file
open OUT, ">", "$outfile.TempCollection" or die $!;

# print header
print OUT getName($infiles[0]);
for(my $i = 1; $i <= $#infiles; $i++){  # for each infile
  print OUT "\t".getName($infiles[$i]);  # print the count for that file
}
print OUT "\n";

foreach my $key (sort keys %collection){  # for each mirBase ID

  print OUT $key;  # print the ID
  for(my $i = 0; $i <= $#infiles; $i++){  # for each infile
    print OUT "\t$collection{$key}[$i]";  # print the count for that file
  }
  print OUT "\n";  # new row when all infile counts are printed
}


# run the R script



### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (path/to,file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
