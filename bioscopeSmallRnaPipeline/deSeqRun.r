# First read in the arguments listed at the command line
args=(commandArgs(TRUE))

# args is now a list of character vectors
# check to see if arguments are passed.
if(length(args)==0){
    print("No arguments supplied.")
}

# get the arguments
countsFile = args[1]
conds = eval(parse(text=args[2]))
out = args[3]



# for debugging only
if(FALSE){
  rm(list=ls())
  gc()
  countsFile = '/home/dahlo/glob/work/testarea/smallRnaDegSeq/all.TempCollection'
  conds = c('A5','B6','A5','B6')
  out = '/home/dahlo/glob/work/testarea/smallRnaDegSeq/deSeqRes.txt'
}

# open the files
geneTable = read.table(file=countsFile,sep='\t',header=T,row.names=1)


## perform calcs
library(DESeq)

## on genes
cds1 <- newCountDataSet( geneTable, conds )
cds1 <- estimateSizeFactors( cds1 )
cds1 <- estimateVarianceFunctions( cds1 )
geneRes <- nbinomTest( cds1, "A5", "B6")
vsd1 <- getVarianceStabilizedData( cds1 )
dists1 <- dist( t( vsd1 ) )
png('geneCountsCluster_ens.png')
heatmap( as.matrix( dists1 ), symm=TRUE )
dev.off()

# merge results
comb = cbind(geneRes,geneTable)
colnames(comb) = c(colnames(geneRes),colnames(geneTable))

# sort increasingly on p-value
comb = comb[order(comb$pval,decreasing=FALSE),]

# write to file
write.table(comb,file=out,sep='\t',row.names=FALSE,col.names=TRUE,quote=FALSE)
