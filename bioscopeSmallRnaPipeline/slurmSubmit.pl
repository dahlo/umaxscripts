#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <path to smallRnaPipeline output dir>
# ex.
# perl scriptname.pl 

use warnings;
use strict;
use File::Path;
use Cwd;
use Switch;

=pod

This script is a wrapper for the small RNA pipeline from Applied Biosystems
to run it on a SLURM system instead of the default PBS.

It takes the usual parameters required by the pipeline and launches
an additional script to handle the SLURM part at the end.

=cut

# define messages
my $usage = "Usage:  perl scriptname.pl <path to smallRnaPipeline output dir>\n";


# get arguments (to get it the way i want it..)
my $config;
my $corona;
my $reads;
my $outdir;


# get arguments
for(my $i = 0; $i <= $#ARGV; $i++){
  switch($ARGV[$i]){
    case "-c"       { $config = absPath($ARGV[$i + 1]) }
    case "-corona"  { $corona = absPath(remSlash($ARGV[$i + 1])) }
    case "-r"       { $reads = absPath($ARGV[$i + 1]) }
    case "-o"       { $outdir = absPath(remSlash($ARGV[$i + 1])) }
    else            { die "Invalid option: $ARGV[$i]\n\n$usage\n" }
  }
  $i++;
}

#### launch the small RNA pipeline ####
system("perl $corona/bin/RNA_matching_analysis_pipeline.pl -r $reads -c $config -corona $corona -o $outdir");




#### start the SLURM part ####

# open the jobinfo file
open JLIST, "<", "$outdir/JOB_LIST.txt" or die "Can not find the jobinfo.txt file:\n$!";

# parse the file and submit jobs
my $depend = "";
my $dependCollect = "--dependency=afterok:";

while(<JLIST>){

  if($_ =~ /^\d+\s+echo/){  # if it is a echo line
 
    chop($dependCollect);  # remove last :
    $depend = $dependCollect;  # save the current list
    $dependCollect = "--dependency=afterok:";  # reset  

  }else{  # if not..
    $_ =~ /^\d+\s+(\S+)/;  # get the scripts full path
    my $fileFull = $1;
    my $fileName = getName($fileFull);

    open SBATCH, ">", "$outdir/sbatch.tmp" or die $!;
    
    # write submission file
    print SBATCH <<EOF;
#!/bin/bash -l

#SBATCH -A b2010033
#SBATCH -p core
#SBATCH -t 24:00:00
#SBATCH -J smallRnaPipeline
#SBATCH -o $fileFull.out
#SBATCH -e $fileFull.err

cd $outdir

sh $fileFull

EOF
    
    # submit and save the job id
    open(JID,"sbatch $depend $outdir/sbatch.tmp | ") or die $!;  # submit file and save response
    #system("scancel -u dahlo");
    <JID> =~ /Submitted batch job (\d+)/;  # get the job id
    $dependCollect .= "$1:";  # store the job id
    my $jobId = $1;
    
    # print status
    print "Submitted $fileFull as $jobId\n";
  }
}

# clean up stuff
#system("rm $outdir/scripts/*.txt");  # empty PBS files




### Methods

# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){ # if the last char is a /
    chop($str);
  }
  return $str;
}

# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}

sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}
