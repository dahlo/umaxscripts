#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile prefix>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "Usage:  perl scriptname.pl <infile> <outfile prefix>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# do the converstion

print "Step 1/2: Sorting BAM file START\n";
system("samtools sort $infile $outfile.sorted");
print "Step 1/2: Sorting BAM file DONE\n";

print "Step 2/2: Indexing BAM file START\n";
system("samtools index $outfile.sorted.bam");
print "Step 2/2: Indexing BAM file DONE\n";

print "Converstion done!\n";
