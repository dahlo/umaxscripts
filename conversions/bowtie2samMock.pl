#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "Usage:  perl scriptname.pl <infile> <outfile>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", $outfile or die $!;

while(<IN>){
  
  # pick out the needed values
  if($_ =~ m/^(.*)([+-])\t(.*)\t(.*)\t.*\t.*\t.*\t.*/){
    
    # write the out file
    print OUT "chr$3\t$2\t$4\t".($4+48)."\n";
    
  }
}
