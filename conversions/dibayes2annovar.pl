#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile> <to plot or not to plot 1/0>
# ex.
# perl scriptname.pl infile outfile 1

use warnings;
use strict;

=pod

=cut

# define messages
my $usage = "dibayes2annovar.pl usage:  perl scriptname.pl <infile> <outfile> <to plot or not to plot 1/0>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;
my $plot = shift or die $usage;

# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", $outfile or die $!;
open R, ">", "$outfile.tmp.R" or die $!;

my @dataSeries;
while(<IN>){
  
  # pick out the needed values
  if($_ =~ m/chr(\d+)\s+\w+\s+\w+\s+(\d+)\s+(\d+).+genotype=(\w);reference=(\w).*refAlleleCounts=(\d+).*novelAlleleCounts=(\d+).*/){
    
    # write the out file, and add the allele ratio at the end
    my $trans4 = translate($4,$5);
  
    print OUT "$1\t$2\t$3\t$5\t$trans4\t$6\t$7\t".($7/($6+$7))."\n";
    
    push(@dataSeries,($7/($6+$7)));
    
  }
}

# plot a nice histogram if it is wanted
if($plot == 1){

  # store the data as a string
  my $data = join(",",@dataSeries);
  
  # write a r script.. overkill perhaps?
  print R <<EOF;
  pdf(file='$outfile.alleleFreq.pdf')
  hist(c($data), breaks=50, main="Allele frequence ratio", xlab="Allele frequence ratio",sub="ratio=1 => only novel allele, ratio=0 => only reference allele")
  dev.off()
EOF

  # execute the r script
  system("R CMD BATCH $outfile.tmp.R");
  system("rm $outfile.tmp.R*");
}



### methods

# translate the iupac symbols to a single base
sub translate{
  
  my $iupac = shift or die $!;
  my $ref = shift or die $!;
  
  # check if a translation is needed
  if($iupac =~ m/[ATCG]/){
    
    # the base is already in a nucleotide format, no translation needed
    return $iupac;
    
  # the base is in 
  }elsif($iupac eq 'R'){
    # return the correct base
    return $ref eq 'A' ? 'G' : 'A';
    
  }elsif($iupac eq 'Y'){
    # return the correct base
    return $ref eq 'C' ? 'T' : 'C';
    
  }elsif($iupac eq 'S'){
    # return the correct base
    return $ref eq 'G' ? 'C' : 'G';
    
  }elsif($iupac eq 'W'){
    # return the correct base
    return $ref eq 'A' ? 'T' : 'A';
    
  }elsif($iupac eq 'K'){
    # return the correct base
    return $ref eq 'G' ? 'T' : 'G';
    
  }elsif($iupac eq 'M'){
    # return the correct base
    return $ref eq 'A' ? 'C' : 'A';

  }else{
    die $!
  }
}

