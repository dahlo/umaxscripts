#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "perl scriptname.pl <infile> <outfile>\n";
my $scripts = "/home/dahlo/glob/work/uppmaxScripts";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# do the converstion

# gtf to bed
system("perl $scripts/conversions/gtf2bed.pl $infile > $outfile.convTemp1");

# sort bed
system("sort -k1,1 -k2,2n $outfile.convTemp1 > $outfile.convTemp2");

# bed to bigBed
system("$scripts/conversions/bedToBigBed $outfile.convTemp2 $scripts/ucscVis/hg19.chrom.sizes $outfile");

# remove temp files
system("rm $outfile.convTemp*");
