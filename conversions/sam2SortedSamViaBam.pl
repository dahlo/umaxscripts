#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "Usage:  perl scriptname.pl <infile> <outfile> <1 to keep intermediate bam file, 0 to not>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;
my $keeper = shift or $usage;


# do the converstion
print "Step 1/4: Converting to BAM format START\n";
system("samtools view -bS $infile > $outfile.TMP1");
print "Step 1/4: Converting to BAM format DONE!\n";

print "Step 2/4: Sorting BAM file START\n";
system("samtools sort $outfile.TMP1 $outfile.TMP2");
print "Step 2/4: Sorting BAM file DONE\n";

#print "Step 3/4: Indexing BAM file START\n";
#system("samtools index $outfile.TMP2.bam");
#print "Step 3/4: Indexing BAM file DONE\n";

print "Step 3/4: Converting to SAM file START\n";
system("samtools view -h -o $outfile $outfile.TMP2.bam");
print "Step 3/4: Converting to SAM file DONE\n";

if($keeper == 1){
  print "Step 4/4: Removing temporary files START\n";
  system("mv $outfile.TMP2.bam $outfile.sorted.bam");
  system("rm $outfile.TMP1");
    print "Step 4/4: Removing temporary files DONE\n";
}else{
  print "Step 4/4: Removing temporary files START\n";
  system("rm $outfile.TMP1");
  system("rm $outfile.TMP2.bam");
  print "Step 4/4: Removing temporary files DONE\n";
}

print "Converstion done!\n";
