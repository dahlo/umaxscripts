#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# do the converstion
print "Step 1/1: Converting to BAM format START\n";
system("samtools view -bS $infile > $outfile");
print "Step 1/1: Converting to BAM format DONE\n";

print "Converstion done!\n";
