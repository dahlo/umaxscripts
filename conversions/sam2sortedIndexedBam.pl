#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "perl scriptname.pl <infile> <outfile>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# do the converstion
print "Step 1/4: Converting to BAM format START\n";
system("samtools view -bS $infile > $outfile.TMP");
print "Step 1/4: Converting to BAM format DONE!\n";

print "Step 2/4: Sorting BAM file START\n";
system("samtools sort $outfile.TMP $outfile");
print "Step 2/4: Sorting BAM file DONE\n";

print "Step 3/4: Removing temporary file START\n";
system("rm $outfile.TMP");
print "Step 3/4: Removing temporary file DONE\n";

print "Step 4/4: Indexing BAM file START\n";
system("samtools index $outfile.bam");
print "Step 4/4: Indexing BAM file DONE\n";

print "Converstion done!\n";
