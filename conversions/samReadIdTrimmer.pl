#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", $outfile or die $!;

while(<IN>){
  
  # pick out the needed values
  if($_ =~ m/^\/bubo\/glob\/g18\/dahlo\/work\/projects\/10ab_solidTranscriptomeSnp\/bwa\/reads\:(\S+)(\t.*)$/){
    # write the out file
    print OUT "$1$2\n";
  }else{
    print OUT $_;
  }
}
