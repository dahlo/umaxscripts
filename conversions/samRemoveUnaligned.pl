#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# do the converstion
print "Step 1/1: Removing unaligned reads START\n";
system("awk '$2 != 4 {print $0}' $infile > $outfile");
print "Step 1/1: Removing unaligned reads DONE\n";

print "Operation complete!\n";
