#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile> <quality file> <index file> <uppmax project id>
# ex.
# perl scriptname.pl infile outfile qualfile indexfile b20110216

use warnings;
use strict;
use File::Path;
use Cwd;


# define messages
my $usage = "perl scriptname.pl <infile> <outfile> <quality file> <index file> <uppmax project id>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;
my $qual = absPath(shift) or die $usage;
my $index = absPath(shift) or die $usage;
my $uid = shift or die $usage;

print "$qual\n\n$index\n";

# create the directories (maybe not all needed?)
my $out = absPath(getPath($outfile))."/ma2bamTempDir";  # get the absolute path to the output file
mkpath("$out/config");
mkpath("$out/intermediate/maToBam");
mkpath("$out/tmp");
mkpath("$out/log");
mkpath("$out/output/maToBam");
mkpath("$out/output/s_mapping");

# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", "$out/output/s_mapping/$outfile.ma" or die $!;

# go through the file line by line
while(<IN>){
  
  # if a header row is found
  if($_ =~ m/^>/){
    my @line = split(',',$_);  # split it on comma signs
    my $id = shift(@line);  # remove the first part, which is the ID of the read
    
    my $q = 40; 
    #~ # check if there are multiple IDs
    if($#line != 0){
      $q = 0;
    }
    
    my $rest = "";
    # for each remaining ID
    for(my $i = 0; $i <= $#line; $i++){
      $line[$i] =~ m/(\d+)_(-?\d+).(\d+).(\d+)/;   # pick out the info
      $rest .= ",$1_$2.0:($4.$3.0):q$q";  # construct the rest of the line
    }
    print OUT "$id$rest\n";  # print the result
    
  }else{ # just print it unmodified
    print OUT $_;
  }
}

### Create config files

# open file handles
open PLAN, ">", "$out/config/analysis.plan" or die $!;
open MAP, ">", "$out/config/Mapping.ini" or die $!;
open SBATCH, ">", "$out/submit.sbatch" or die $!;


# write analysis.plan
print PLAN <<EOF;

$out/config/Mapping.ini

EOF


# write Mapping.ini
print MAP <<EOF;
pipeline.cleanup.middle.files=1
pipeline.cleanup.temp.files=1


#    Global Parameters 

run.name=MaToBamRun
sample.name=MaToBamRun
reference=$index
output.dir=$out/output
tmp.dir=$out/tmp
intermediate.dir=$out/intermediate
log.dir=$out/log
scratch.dir=/scratch/solid

######################################
##  ma.to.bam.run
######################################
ma.to.bam.run=1
mapping.output.dir=$out/output/s_mapping
ma.to.bam.qual.file=$qual
ma.to.bam.reference=$index
ma.to.bam.output.dir=$out/output/maToBam
ma.to.bam.match.file=$out/output/s_mapping/$outfile.ma
ma.to.bam.output.file.name=$out/output/maToBam/$outfile.ma.bam
ma.to.bam.intermediate.dir=$out/intermediate/maToBam
ma.to.bam.temp.dir=$out/output/temp/maToBam
ma.to.bam.pas.file=
ma.to.bam.output.filter=primary
ma.to.bam.correct.to=reference
ma.to.bam.clear.zone=5
ma.to.bam.mismatch.penalty=-2.0
ma.to.bam.library.type=fragment
ma.to.bam.library.name=lib1
ma.to.bam.slide.name=
ma.to.bam.description=
ma.to.bam.sequencing.center=freetext
ma.to.bam.tints=agy
ma.to.bam.base.qv.max=40
ma.to.bam.temporary.dir=

EOF

# execute the run
print SBATCH <<EOF;
#!/bin/bash -l
source /etc/profile
module load bioinfo-tools
module load bioscope

# run bioscope in the background, and let it work its magic
nohup sh bioscope.sh -A $uid -l $out/log/log.log $out/config/analysis.plan
EOF

#system("sh $out/$stamp.sbatch &");
#system("rm $out/$stamp.sbatch");
print "To start the alignment, please submit your file to the queue system by typing:\nsh $out/submit.sbatch &\n";





### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (file.txt,path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
