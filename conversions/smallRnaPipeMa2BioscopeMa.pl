#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "perl scriptname.pl <infile> <outfile>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", $outfile or die $!;

# go through the file line by line
while(<IN>){
  
  # if a header row is found
  if($_ =~ m/^>/){
    my @line = split(',',$_);  # split it on comma signs
    my $id = shift(@line);  # remove the first part, which is the ID of the read
    
    my $q = 40; 
    #~ # check if there are multiple IDs
    if($#line != 0){
      $q = 0;
    }
    
    my $rest = "";
    # for each remaining ID
    for(my $i = 0; $i <= $#line; $i++){
      $line[$i] =~ m/(\d+)_(-?\d+).(\d+).(\d+)/;   # pick out the info
      $rest .= ",$1_$2.0:($4.$3.0):q$q";  # construct the rest of the line
    }
    print OUT "$id$rest\n";  # print the result
    
  }else{ # just print it unmodified
    print OUT $_;
  }
}
