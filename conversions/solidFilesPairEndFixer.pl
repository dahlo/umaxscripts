#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "\n";

# get the file names
my $in1 = shift or die $usage;
my $in2 = shift or die $usage;
my $out1 = shift or die $usage;
my $out2 = shift or die $usage;

# open file handles
open IN1, "<", $in1 or die $!;
open IN2, "<", $in2 or die $!;
open OUT1, ">", $out1 or die $!;
open OUT2, ">", $out2 or die $!;


my $row1 = <IN1>;
my $row2 = <IN2>;
my ($id11, $id12, $id13, $id21, $id22, $id23, $temp);
my $run = 1;
my $c = 0;
while($run){

  ## speedometer
  #if($c % 10000 == 0){  # print every 10 000 lap
    #print "$c\n";
#    
  #}
  #$c++;
  
  # check if any of the files are empty
  if(eof(IN1) || eof(IN2)){
    exit;
  }

  # if it is a valid fasta header
  if($row1 =~ /^>(\d+)_(\d+)_(\d+)/){
    $id11 = $1;  # save the id
    $id12 = $2;
    $id13 = $3;
    
    
    # if it is a valid fasta header
    if($row2 =~ /^>(\d+)_(\d+)_(\d+)/){
      $id21 = $1;  # save the id
      $id22 = $2;
      $id23 = $3;
      
      #print "$id11.$id12.$id13\t$id21.$id22.$id23\n";
      
      # ok, both files are on a valid fasta header. Let the comparing begin!
      
      # check first pair
      if($id11 == $id21){  # if first pair is identical
        
        # check second pair
        if($id12 == $id22){  # if second pair is identical
          
          # check third pair
          if($id13 == $id23){  # if third pair is identical
            
            # all pairs are identical, match is found
            # print read and qual to out files
            print OUT1 ">$id11\_$id12\_$id13\n";
            $row1 = <IN1>;
            print OUT1 $row1;
            
            print OUT2 ">$id21\_$id22\_$id23\n";
            $row2 = <IN2>;
            print OUT2 $row2;            
     
          }elsif($id13 < $id23){  # if third pair is smaller in file1
            $row1 = <IN1>;  # take a step in file1
            
          }elsif($id13 > $id23 ){ # if third pair is larger in file1
            $row2 = <IN2>;  # take a step in file2
            
          }
          
   
        }elsif($id12 < $id22){  # if second pair is smaller in file1
          $row1 = <IN1>;  # take a step in file1
          
        }elsif($id12 > $id22 ){ # if second pair is larger in file1
          $row2 = <IN2>;  # take a step in file2
          
        }
        
 
      }elsif($id11 < $id21){  # if first pair is smaller in file1
        $row1 = <IN1>;  # take a step in file1
        
      }elsif($id11 > $id21 ){ # if first pair is larger in file1
        $row2 = <IN2>;  # take a step in file2
        
      }

      
    }elsif($row2 =~ /^#/){  # check if it is a comment row
  
      # print directly
      print OUT2 $row2;
      $row2 = <IN2>;
    
    }else{  # not a valid fasta row
      $row2 = <IN2>;  # take a step in file2
      
    }
  }elsif($row1 =~ /^#/){  # check if it is a comment row
  
    # print directly
    print OUT1 $row1;
    $row1 = <IN1>;
    
  }else{  # not a valid fasta row
    $row1 = <IN1>;  # take a step in file1
  }
}
