#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <>
# ex.
# perl scriptname.pl 

use warnings;
use strict;
use File::Path;
use Cwd;


=pod



=cut

# get variables
my $usage = "Usage:  perl scriptname.pl <cuffdiff file> <annotations file> <output file>\n";
my $cuff = shift or die $usage;
my $annotations = shift or die $usage;
my $out = shift or die $usage;

# open the files
open CUFF, "<", $cuff or die $!;
open ANNO, "<", $annotations or die $!;
open OUT, ">", $out or die $!;

# read the annotations to memory
my @line;
my %anno;
<ANNO>; # remove header
while(<ANNO>){
  @line = split('\t',$_); # divide the line into array elements
  $anno{$line[1]} = [@line]; # save the array     [@line] similar but not same as \@line
}

# go through the cuffdiff results
@line = split('\t',<CUFF>); # divide the line into array elements
my $ID = shift(@line); # remove the ID temporarily
unshift(@line,"listName"); # attach the list the ID is annotated in
unshift(@line,"geneName"); # attach the gene name
unshift(@line,$ID); # attach the ID again
print OUT join("\t",@line); # print the result

while(<CUFF>){
  @line = split('\t',$_); # divide the line into array elements
  
  if(defined $anno{$line[0]}){ # if there is a annotation for it (weird if not?)
    $ID = shift(@line); # remove the ID temporarily
    unshift(@line,$anno{$ID}[0]); # attach the list the ID is annotated in
    unshift(@line,$anno{$ID}[2]); # attach the gene name
    unshift(@line,$ID); # attach the ID again
    print OUT join("\t",@line); # print the result
    
  }else{ # if there is no annotation (weird!)
    $ID = shift(@line);
    unshift(@line,'-');
    unshift(@line,'-');
    unshift(@line,$ID);
    print OUT join("\t",@line);
  }
}











### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (path/to,file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
