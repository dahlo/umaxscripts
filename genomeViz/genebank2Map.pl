#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "Usage:  perl scriptname.pl <infile> <outfile>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", $outfile or die $!;

while(<IN>){
  chomp($_);  # remove newline
  # pick out the needed values
  if($_ =~ m/^LOCUS\s+\w+\s+(\d+) bp/){  # the first line of the file, get the genome length (since it is always the first and only the first line, why have it in the loop? Also, instead of writing a comment complaining about it, why not just fix it?)
    # print the genome lenght to the out file
    print OUT "$1\n";
    
  }elsif($_ =~ m/^\s+CDS\s+(complement\()?(join\()?([\w\.,<]+)(\))?(\))?/){  # if a CDS is found

      my $reg = $3;  # save the region string
  
    # get stand info
    my $strand = '+';
    if(defined $1){  # if it is in the compliment strand
      $strand = '-';
    }
    
    # get protein ID
    my $run = 1;
    my $id;
    while($run){
      my $row = <IN>;
      if($row =~ m/\/protein_id="(.+)"/){
        $id = $1;
        $run = 0;
      }
    }

    # get all the regions
    my @regions = split(/,/,$reg);
    
    # for each found region, create an entry
    foreach my $region (@regions){
      $region =~ m/(\d+)\.\.(\d+)/;
      #print "$region\n";
      print OUT "$id\tCDS\t$strand\t$1\t$2\t$id\n";
      #print "$1\t$2\n";
    }
    
    
    
  }else{  # maybe nothing?
    
  }
}
