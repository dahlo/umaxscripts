#!/usr/bin/pyton
#
# Author: Martin Dahlo
#
# Used to setup the correct folder structure for running
# LifeScope in a project
#
# Usage: lifescope-setup <project id>
# Ex.
# lifescope-setup b2010074

# load libraries
import sys
import os
from stat import *




# define usage message
usage = "Usage: lifescope-setup <project id>"

# check number of arguments
if len(sys.argv) != 2 :
    sys.exit(usage)

# get the arguments
proj = sys.argv[1]


# check that user has write permissions in the destination folder
if not os.path.exists("/bubo/proj/%s/" % proj):
    sys.exit("No such project volume: /bubo/proj/%s/" % proj)
elif not os.access(r"/bubo/proj/b2010074/", os.W_OK):
    sys.exit("No write permission: /bubo/proj/%s/" % proj)
    

# create dirs if not already created
if not os.path.exists("/bubo/proj/%s/lifescope/output" % proj):
    os.makedirs("/bubo/proj/%s/lifescope/output" % proj)

if not os.path.exists("/bubo/proj/%s/lifescope/input" % proj):
    os.makedirs("/bubo/proj/%s/lifescope/input" % proj)




# set ownership and permissions on folders
# 
# user:lscope
# input: 650
# output: 620

os.system("chown -R %s:%s /bubo/proj/%s/lifescope/" % (os.getusername(), "lscope", proj)




# remove link if it already exists (in the unlikely event that someone is trying to sabotage by creating misdirecting links..)
if os.path.islink("/bubo/sw/apps/bioinfo/lifescope/lifescope2.1/lifescope/projects/%s" % proj):
    os.unlink("/bubo/sw/apps/bioinfo/lifescope/lifescope2.1/lifescope/projects/%s" % proj)

# create a new link
os.symlink("/bubo/proj/%s/lifescope/output/" % proj, "/bubo/sw/apps/bioinfo/lifescope/lifescope2.1/lifescope/projects/%s" % proj)
    
