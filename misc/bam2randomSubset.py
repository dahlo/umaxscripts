#!/usr/bin/pyton
#
# Author: Martin Dahlo
#
# Used to randomly sample a bam file with a specified frequency
# Usage: python bam2randomSubset.py <infile> <outfile> <frequency, [0,1]>

# load libraries
import sys
import os
import pysam
import random


# define usage message
usage = "Usage: python bam2randomSubset.py <infile> <outfile> <frequency, [0,1]>"
#~ 
# check number of arguments
if len(sys.argv) != 4 :
    sys.exit(usage)
#~ 
# get the arguments
inName = sys.argv[1]
outName = sys.argv[2]
freq = float(sys.argv[3])

# load the samtools module
#os.system("module sh load bioinfo-tools samtools")

# open files
bamFile = pysam.Samfile(inName, 'rb')
out = pysam.Samfile(outName, 'wb', header = bamFile.header)

count_all = 0
count_chosen = 0
# go through the reads
for read in bamFile:
    count_all += 1
    # if the random number is less than the given p-value, keep it.
    if random.random() < freq:
        out.write(read)
        count_chosen += 1

print str(count_chosen) + " / " + str(count_all) + " (" + str(float(count_chosen)/float(count_all)) + "%) reads have been extracted."
