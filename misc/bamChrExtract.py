#!/usr/bin/pyton
#
# Author: Martin Dahlo
#
# Used to randomly sample a bam file with a specified frequency
# Usage: python bam2randomSubset.py <infile> <outfile> <frequency, [0,1]>

# load libraries
import sys
import os
import pysam


# define usage message
usage = "Usage: python bam2randomSubset.py <infile> <outfile> <frequency, [0,1]>"

# check number of arguments
if len(sys.argv) != 4 :
    sys.exit(usage)

# get the arguments
inName = sys.argv[1]
outName = sys.argv[2]
chr = sys.argv[3]

# open files
bamFile = pysam.Samfile(inName, 'rb')
out = pysam.Samfile(outName, 'wb', header = bamFile.header)

fetched = 0
# get all the reads from the specified chromosome
for read in bamFile.fetch(chr):
        # write to outfile
        out.write(read)
        fetched = fetched + 1

print str(fetched) + " reads have been extracted."
