#!/usr/bin/perl

use warnings;
use strict;
use Cwd 'abs_path';
use Parallel::ForkManager;

# get the dir of the script
use FindBin qw($Bin);

open(NR,"cat /proc/cpuinfo | grep processor | wc -l |") || die "Failed: $!\n";
my $nr = <NR>;
chomp($nr);
$nr++;

# calculate the multiplier to reach desired ram usage
my $ram = $ARGV[0]/($nr-1);

my $fork_manager = Parallel::ForkManager->new($nr);

for(my $i = 0; $i < $nr; $i++){
  # Forks and returns the pid for the child:
  my $pid = $fork_manager->start and next; 

  system("Rscript $Bin/busy.r $ram &> /dev/null");

  $fork_manager->finish; # Terminates the child process
}

$fork_manager->wait_all_children;

