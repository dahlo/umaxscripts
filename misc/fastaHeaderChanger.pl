#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <infile> <outfile>
# ex.
# perl scriptname.pl infile outfile

use warnings;
use strict;


# define messages
my $usage = "Usage:  perl scriptname.pl <infile> <outfile>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", $outfile or die $!;


# go through the file
while(<IN>){
  
  # pick out the needed values
  if($_ =~ m/^>(chr)*(\w+)/){  # if it is a fasta header row
    print OUT ">$2\n";

  }else{  # if it is not..
    print OUT $_;
  }
}
