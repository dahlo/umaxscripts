#!/bin/env python

from __future__ import print_function
import os
import sys
from IPython.core.debugger import Tracer
import signal
import operator




usage = "python {} <fastq file>".format(sys.argv[0])

try:
	filename = sys.argv[1]
except:
	sys.exit(usage)






systems = {	'Sanger':[33+0,33+40], 
			'Solexa':[64+-5,64+40],
			'Illumina 1.3+':[64+0,64+40],
			'Illumina 1.5+':[64+3,64+40],
			'Illumina 1.8+':[33+0,33+41],
			}




def exclude_systems(systems, min_val, max_val):

	updated = 0
	for name,system in systems.items():

		# Tracer()()
		if min_val < system[0]:
			del systems[name]
			updated = 1
		elif max_val > system[1]:
			del systems[name]
			updated = 1


	if len(systems) == 0:
		sys.exit("ERROR: This file does not match any of the defined systems. Maybe there is a new system that is not defined in this program?\nFinal min/max: {},{}".format(min_val, max_val))

	if updated:
		print("\nNew min/max detected: {},{}  Remaining valid systems are:".format(min_val, max_val))
		for name,system in systems.items():
			print("{}\t{},{}".format(name, system[0], system[1]))

		print('')

	else:
		print("New min/max detected: {},{} but no exclusion could be done based on the new values.".format(min_val, max_val))

	return systems



def signal_handler(signal, frame):
	print()
	final_report()
	sys.exit(0)


def final_report():

	print('\nI could only find quality scores between {} and {}, which match the following systems:'.format(min_val, max_val))
	for name,system in systems.items():
			print("{}\t{},{}".format(name, system[0], system[1]))

	best_guess()


def best_guess():

	print("\nI tried to score the remaining systems after how large of an overlap they have with the observed min/max values. Higher is better:")

	scores = {}
	for name,system in systems.items():
		scores[name] = float(max_val-min_val)/float(system[1]-system[0])


	for name,score in sorted(scores.items(), key=operator.itemgetter(1)):
		# Tracer()()
		print("{}\t{} overlap.".format(name, score))






signal.signal(signal.SIGINT, signal_handler)
# get the initial character from the file to initialize the min and max
with open(filename) as file:
	counter = 1
	for line in  file:
		if counter % 4 == 0:
			min_val = ord(line[0])
			max_val = ord(line[0])
			break
		counter += 1


with open(filename) as file:

	counter = 1

	for line in file:

		# only every 4th line in the fastq file contains qualities
		if counter % 4 == 0:

			if counter % 1000 == 0:
				print("Processing read {}. Press ctrl+c when you think it has run for long enough without finding the final answer.".format(counter), end='\r')


			for base in list(line.strip()):

				# check if new low
				if ord(base) > max_val:
					max_val = ord(base)

					# see if any system can be excluded
					systems = exclude_systems(systems, min_val, max_val)

				# check if new high
				elif ord(base) < min_val:
					min_val = ord(base)

					# see if any system can be excluded
					systems = exclude_systems(systems, min_val, max_val)

		counter += 1
signal.pause()






