#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <list1> <list2> <out prefix>
# ex.
# perl scriptname.pl 

use warnings;
use strict;
use File::Path;
use Cwd;
use List::Compare;


=pod



=cut

# get variables
my $usage = "Usage:  perl scriptname.pl <list1> <list2> <output prefix>\n";
my $l1 = shift or die $usage;
my $l2 = shift or die $usage;
my $out = shift or die $usage;

# open the files
open L1, "<", $l1 or die $!;
open L2, "<", $l2 or die $!;
open INTER, ">", "$out.inter" or die $!;
open UNION, ">", "$out.union" or die $!;
open L1U, ">", "$out.l1" or die $!;
open L2U, ">", "$out.l2" or die $!;
open STAT, ">", "$out.stat" or die $!;

# read list 1
my @l1;
while(<L1>){
  chomp($_);
  push(@l1,$_);
}

# read list 2
my @l2;
while(<L2>){
  chomp($_);
  push(@l2,$_);
}


# create the compare object
my $lc = List::Compare->new(\@l1, \@l2);

# get the lists
my @inter = $lc->get_intersection;
my @union = $lc->get_union;
my @l1u = $lc->get_unique;
my @l2u = $lc->get_complement;

# print the result
print INTER join("\n",@inter);
print UNION join("\n",@union);
print L1U join("\n",@l1u);
print L2U join("\n",@l2u);

# print stats
my $interp = $#inter/$#union*100;
my $l1p = $#l1u/$#union*100;
my $l2p = $#l2u/$#union*100;
my $l1ofl2 = $#inter/$#l2*100;
my $l2ofl1 = $#inter/$#l1*100;

print STAT <<EOF;
List1:\t$#l1
List2:\t$#l2


Union:\t\t$#union\t( 100 % )
Intersect:\t$#inter\t( $interp % )
List1Unique:\t$#l1u\t( $l1p % )
List2Unique:\t$#l2u\t( $l2p % )


List1 contains % of list2:\t$l1ofl2 %
List2 contains % of list1:\t$l2ofl1 %
EOF







### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (path/to,file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the last element first (the file name), and then the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
