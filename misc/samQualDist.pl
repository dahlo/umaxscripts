#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <>
# ex.
# perl scriptname.pl 

use warnings;
use strict;


=pod

Used to generate statistics of the quality scores in a sam file

=cut


# get variables
my $usage = "Usage:  perl scriptname.pl <infile> <quality identifier (i = illumina 1.3+ (phred+64), s = sanger (phred+33))>\n";
my $sam = shift or die $usage;
my $qual = shift or die $usage;

# determine the quality offset
# If GATK has been used, it converts the quality to sanger format.. Without telling you, thank you very much
if($qual eq 'i'){
  $qual = 64;
}elsif($qual eq 's'){
  $qual = 33;
}else{
  die "Unknown quality identifier. Valid types are i and s, for illumina and sanger.\n(phred+64 and phred+33 respectivly)\n";
}

# open the files
open SAM, "<", $sam or die $!;
#open STAT, ">", "$sam.qualStat" or die $!;


# initiate
my @hist = ((0) x 150);
my @line;
my $number;

# go thorugh the file
while(<SAM>){
  # get the quality column
  if($_ =~ /^\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s?/){
    
    # split into chars
    @line = split(//,$1);
    
    for(my $i = 0; $i <= $#line; $i++){ # for each char
    
      $number = ord($line[$i]); # get the number represented by the ascii char
      $hist[$number] += 1;
      
    }
  }
}

# check interval limits
my $min =  99999;
my $max = -99999;
for(my $i = 0; $i <= $#hist; $i++){ # go through the histogram array
  if($hist[$i] != 0){ # if the value has been observed
    
    if($i < $min){ $min = $i; } # change lower limit
    if($i > $max){ $max = $i; } # change upper limit
    
  }
}

# plot a nice histogram over the indel lengths
if(1){

  # store the data as a string
  my $dataAll = join(",",@hist[$min..$max]);
  my $names = join(",",($min..$max));

  
  # write a r script.. overkill perhaps?
  open R, ">", "$sam.rmpR" or die $!;
  print R <<EOF;
  pdf(file='$sam.histogram.pdf')
  
  data = c($dataAll)
  names = c($names)-$qual
  mean = sum(names*data)/sum(data)
  
  barplot(data, main="Quality score distribution (basewise)", sub=paste("$sam (mean = )",mean,sep=''), xlab="Phred score", ylab="Frequency", names.arg=names)
  dev.off()
EOF

  # execute the r script
  system("R CMD BATCH $sam.rmpR");
  #system("rm $sam.rmpR*");
}
