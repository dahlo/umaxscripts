#!/usr/bin/python
#
# Author: Martin Dahlo
#
# Used to format an csv-file
#
# Usage: python parse.py <input file> <output file>  <read lenght> <coverage degree>
# Ex.
# python parse.py ref.fa simRun.fq 75 30

# load libraries
import sys
import os
import string
import re
import random

def sim(collection,outName,header):
    
    outFile = open(outName,'a')
    
    # get the length of the string
    stop = int(len(collection) - rleng)
    
    # calculate the number of reads needed to get specified coverage
    numReads = int(cov * stop / rleng)
    
    # generate a random list of start points
    starts = [random.randint(0,stop) for r in xrange(0,numReads)]
    
    # create each read
    i = 0
    for start in starts:
        id = str(i).zfill(15)
        outFile.write("@%s-%s\n%s\n+%s-%s\n%s\n" % (header, id, collection[start:start+rleng], header, id, "B"*rleng))
        #~ print "@%s-%s\n%s\n+%s-%s\n%s\n" % (header, id, collection[start:start+rleng], header, id, "B"*rleng)
        #~ print start
        #~ print start+rleng
        i += 1
    
    



# define usage message
usage = "Usage: python simSeq.py <input file> <output file> <read lenght> <coverage degree>"

# check number of arguments
if len(sys.argv) != 5 :
		sys.exit(usage)

# get the arguments
inName = sys.argv[1]
outName = sys.argv[2]
rleng = int(sys.argv[3])
cov = float(sys.argv[4])


# open the files
inFile = open(inName, 'r') # for reading

# get the first header
header = inFile.readline().strip()
match = re.search(r"^>(.+)",header)
header = match.group(1)

# add a random string to it, if files are to be concatinated later on
header += '-' + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))


# initiate
collection = ""
open(outName,'w').close()

for line in inFile:
    
    # remove newline
    line = line.strip()
    
    # check if it is a new header
    match = re.search(r'^>(.+)',line)
    if match:
        sim(collection,outName,header)
        collection = ""
        header = match.group(1)
        
    # continue collecting current 
    else:
        collection += line
        
        

# if it was the last fasta thingy
sim(collection,outName,header)
