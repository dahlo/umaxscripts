#!/usr/bin/python
#
# Author: Martin Dahlo
#
# Used to generate fake sequencing data from a sequence
#
# Usage: python simseq_v2.py <input file> <output file>  <read lenght> <coverage degree>
# Ex.
# python simseq_v2.py ref.fa simRun.fq 75 30

# load libraries
import sys
import os
import string
import re
import random

def sim(copy1, copy2, average_coverage, outName, header):
    
    outFile = open(outName,'a')

    # package the chromosome
    chromosome = [copy1, copy2]
    
    # get the length of the string
    seq_length = int(len(copy1) - read_length)
    
    # calculate the number of reads needed to get specified coverage
    numReads = int(average_coverage * seq_length / read_length)
    
    # generate a random list of start points
    starts = [random.randint(0,seq_length) for r in xrange(0,numReads)]
    
    # create each read
    i = 0
    for start in starts:
        id = str(i).zfill(15)
        outFile.write("@%s-%s\n%s\n+%s-%s\n%s\n" % (header, id, chromosome[i%2][start:start+read_length], header, id, "B"*read_length))

        i += 1
    
    






def mutate(sequence, mutation_rate):

    sequence = list(sequence)

    for i in range(0,len(sequence)):

        # increase the mutation rate with 1/3 since  well mutate the base to the same base as it already is in 1/4 of the cases
        if random.random() < (mutation_rate / 10000)*(1.0+1.0/3.0):

            sequence[i] = random.choice(['A','T','G','C'])


    return "".join(sequence)









# define usage message
usage = "Usage: python simSeq.py <input file> <output file> <read lenght> <coverage degree> <snp rate n/10kb>"

# check number of arguments
if len(sys.argv) != 6 :
		sys.exit(usage)

# get the arguments
inName = sys.argv[1]
outName = sys.argv[2]
read_length = int(sys.argv[3])
average_coverage = float(sys.argv[4])
snp_rate = float(sys.argv[5])



# open the files
inFile = open(inName, 'r') # for reading

# get the header
header = inFile.readline().strip()
match = re.search(r"^>(.+)",header)
header = match.group(1)

# add a random string to it, if files are to be concatinated later on
header += '-' + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))


# initiate
copy1 = ""

for line in inFile:
    
    # remove newline
    line = line.strip()
    
    # check if it is a new header
    match = re.search(r'^>(.+)',line)
    if match:

        # create homozygote snps
        copy1 = mutate(copy1, snp_rate/3)

        # create the 2nd chromosome copy
        copy2 = copy1

        # create heterozygote snps
        copy1 = mutate(copy1, snp_rate/3)
        copy2 = mutate(copy2, snp_rate/3)


        sim(copy1, copy2, average_coverage, outName, header)
        copy1 = ""
        copy2 = ""
        header = match.group(1)
        
    # continue collecting current 
    else:
        copy1 += line
        
        

### and for the last fasta entry

# create homozygote snps
copy1 = mutate(copy1, snp_rate/3)

# create the 2nd chromosome copy
copy2 = copy1

# create heterozygote snps
copy1 = mutate(copy1, snp_rate/3)
copy2 = mutate(copy2, snp_rate/3)

# simulate the sequencing
sim(copy1, copy2, average_coverage, outName, header)
