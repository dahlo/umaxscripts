#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl webexportCreator.pl <proj id> <username of the owner>
# ex.
# perl webexportCreator.pl b2010074 dahlo

use warnings;
use strict;


=pod

Creates a webexport folder and a link to it, for a specified project.

=cut


# get variables
my $usage = "Usage:  perl webexportCreator.pl <proj id> <username of the owner>\n";
my $proj = shift or die $usage;
my $owner = shift or die $usage;
#my $debug = shift; # only used for debugging


# the path to the projects webexport root
my $webroot = "/bubo/webexport/public/$proj";
my $projroot = "/bubo/proj/$proj";

# change the root path, to enable debugging for non-root users
#if($debug eq 'd'){
#  $webroot = "/home/dahlo/glob/work/testarea/webexport/public/$proj";
#}

# create the directories
print "mkdir -p $webroot/files\n";
system("mkdir -p $webroot/files");
print "mkdir -p $webroot/private\n";
system("mkdir -p $webroot/private");

# print the standard files
open INDEX, ">", "$webroot/index.html" or die $!;
open HTACCESS, ">", "$webroot/private/.htaccess" or die $!;
print "touch $webroot/private/.htpasswd\n";
system("touch $webroot/private/.htpasswd");

print "printing to $webroot/index.html\n";
print INDEX <<EOF;
<html><head><meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"><title>SNIC-UPPMAX File Export Area</title>
</head><body style=" font-family: arial, helvetica, sans-serif; font-size: 12px; padding: 10px; margin: 0;">
<div style="width: 907px; padding: 0pt; margin: 0pt auto; border: 1px solid rgb(204, 204, 204);"><a href="/"><img src="../exportweb_banner.png" style="margin: -1px;" border="0"></a><div style="padding: 12px 248px 12px 136px;"><h2>Web Area for Project $proj</h2>Please contact a project member for assistance.<br><br><br></div><hr style="border-width: 1px 0pt 0pt; border-style: solid; border-color: rgb(204, 204, 204);" size="1"><div style="padding: 7px 248px 12px 136px;">Export server at <a href="http://www.uppmax.uu.se">SNIC-UPPMAX</a>, <a href="http://www.uu.se">Uppsala University</a><br><a href="http://www.uppmax.uu.se/contact-info">Contact
site administrators</a>  <a href="http://www.uppmax.uu.se/faq/running-jobs/how-can-i-make-data-available-over-http">Help for project owners</a></div></div>
</body></html>
EOF
close(INDEX);

print "printing to $webroot/private/.htaccess\n";
print HTACCESS <<EOF;
AuthType Basic
AuthUserFile $webroot/private/.htpasswd
AuthName "Private project area"
require valid-user
EOF
close(HTACCESS);


# create the link from the project folder
print "ln -s $webroot $projroot/webexport\n";
system("ln -s $webroot $projroot/webexport");

# change the owner of the files (maybe changing owner of the link in unnecessary)
print "chown -R $owner:$proj $webroot $projroot/webexport\n\n";
system("chown -R $owner:$proj $webroot $projroot/webexport");

# finished
print "Done!\n";
