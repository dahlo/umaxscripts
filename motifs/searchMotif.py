#!/usr/bin/pyton
#
# Author: Martin Dahlo
#
# Used to search a fasta file for a specific, given, motif
# Usage: python searchMotif.py <infile (fasta)> <out file> <motif> <cutoff value ([0,1])>

# load libraries
import sys
import os
import string
import re




# define the score matix
score = {}

score['A'] = {}
score['T'] = {}
score['G'] = {}
score['C'] = {}

score['A']['A'] = 1
score['A']['T'] = 0
score['A']['G'] = 0
score['A']['C'] = 0
score['A']['R'] = 1
score['A']['Y'] = 0
score['A']['U'] = 0
score['A']['S'] = 0
score['A']['W'] = 1
score['A']['M'] = 1
score['A']['K'] = 0
score['A']['B'] = 0
score['A']['D'] = 1
score['A']['H'] = 1
score['A']['V'] = 1
score['A']['N'] = 1

score['T']['A'] = 0
score['T']['T'] = 1
score['T']['G'] = 0
score['T']['C'] = 0
score['T']['R'] = 0
score['T']['Y'] = 1
score['T']['U'] = 1
score['T']['S'] = 0
score['T']['W'] = 1
score['T']['M'] = 0
score['T']['K'] = 1
score['T']['B'] = 1
score['T']['D'] = 1
score['T']['H'] = 1
score['T']['V'] = 0
score['T']['N'] = 1

score['G']['A'] = 0
score['G']['T'] = 0
score['G']['G'] = 1
score['G']['C'] = 0
score['G']['R'] = 1
score['G']['Y'] = 0
score['G']['U'] = 0
score['G']['S'] = 1
score['G']['W'] = 0
score['G']['M'] = 0
score['G']['K'] = 1
score['G']['B'] = 1
score['G']['D'] = 1
score['G']['H'] = 0
score['G']['V'] = 1
score['G']['N'] = 1

score['C']['A'] = 0
score['C']['T'] = 0
score['C']['G'] = 0
score['C']['C'] = 1
score['C']['R'] = 0
score['C']['Y'] = 1
score['C']['U'] = 0
score['C']['S'] = 1
score['C']['W'] = 0
score['C']['M'] = 1
score['C']['K'] = 0
score['C']['B'] = 1
score['C']['D'] = 0
score['C']['H'] = 1
score['C']['V'] = 1
score['C']['N'] = 1



# define the reverse compliment function
def reversecomplement(sequence):
    """Return the reverse complement of the dna string.""" 

    complement = {"A":"T", "T":"A", "C":"G", "G":"C", "N":"N"}
    
    reverse_complement_sequence = ""

    sequence_list = list(sequence)
    sequence_list.reverse()

    for letter in sequence_list:
        reverse_complement_sequence += complement[letter.upper()]
    
    return reverse_complement_sequence
    
    

# define scoring function
def getScore(seq, motif, cutoff):
    """Return the score of a string.""" 
    
    # define variables as floats
    total = float(0)
    totalRevComp = float(0)
    
    # create the reverse compliment string
    seqRevComp = reversecomplement(seq)
    
    # score each nucleotide in the sequence
    for i in range(len(seq)):
        total += score[seq[i]][motif[i]]
        
    for i in range(len(seqRevComp)):
        totalRevComp += score[seqRevComp[i]][motif[i]]
        
    
    # if one of the values are above the cutoff
    if max(total,totalRevComp)/len(motif) >= cutoff:
        if total < abs(totalRevComp):
            return -1*totalRevComp/len(motif) # return negative if it's on the reversed strand
        else:
            return total/len(motif) # return normal if not
    else:
        return 0 # it was not above the cutoff





# define usage message
usage = "Usage: python searchMotif.py <infile (fasta)> <out file> <motif> <cutoff value ([0,1])>"

# check number of arguments
if len(sys.argv) != 5 :
    sys.exit(usage)

# get the arguments
inName = sys.argv[1]
outPrefix = sys.argv[2]
motif = sys.argv[3].upper()
cutoff = float(sys.argv[4])

# save motif length
motifLength = len(motif)


# break up path and prefix
(path, prefix) = os.path.split(os.path.abspath(outPrefix))

# create dirs if it does not exist
os.system("mkdir -p %s" % path)


# open the files
inFile = open(inName,'r')


# get the fasta header
header = inFile.readline().strip()
match = re.search(r'^>(.+)',header)
id = match.group(1)
seq = ''
scoreCollection = {}

# go through the file and collect the sequence to ram
for line in inFile:
    
    # remove newline
    line = line.strip()
    
    # if next fasta header is found
    match = re.search(r'^>(.+)',line)
    if match:
        
        # save the fasta header and use as id
        scoreCollection[id] = []
        
        
        # score the old sequence
        for i in range(len(seq) - motifLength):
            
            # save start position and score (percent correct)
            substr = seq[ i : (i+motifLength) ]
            
            # get the score and save it if it was above the cutoff limit
            temp = getScore(substr, motif, cutoff)
            if temp > 0:  # if it's on the forward strand, store it
                scoreCollection[id].append([i, '+', temp, substr])
            elif temp < 0:  # if it is on the reversed strand, adjust the start site of the motif
                scoreCollection[id].append([(i +motifLength -1), '-', -temp, reversecomplement(substr)])
            
        
        # save new id and header
        id = match.group(1)
        header = line
        
        
        
        
    # the line is still within the current fasta element, continue collecting
    else:
        seq += line
        

### score the last fasta header
# save the fasta header and use as id
scoreCollection[id] = []


# score the last sequence
for i in range(len(seq) - motifLength):
    
    # save start position and score (percent correct)
    substr = seq[ i : (i+motifLength) ]
    
    # get the score and save it if it was above the cutoff limit
    temp = getScore(substr, motif, cutoff)
    if temp > 0:  # if it's on the forward strand, store it
        scoreCollection[id].append([i, '+', temp, substr])
    elif temp < 0:  # if it is on the reversed strand, adjust the start site of the motif
        scoreCollection[id].append([(i +motifLength -1), '-', -temp, reversecomplement(substr)])
    



# sort and print data to files
for id in scoreCollection.keys():
    
    # open the outfile
    outFile = open("%s_%s_%s" % (outPrefix, motif, id) ,'w')
    
    # print motif info in a header
    outFile.write(">Motif searched for: %s\n" % motif)
    
    # sort the list after score
    scoreCollection[id].sort(lambda x, y: cmp(y[2],x[2]))
    
    
    for x in range(len(scoreCollection[id])):
        outFile.write(str(scoreCollection[id][x][0] + 1) + "\t" + str(scoreCollection[id][x][1]) + "\t" + str(scoreCollection[id][x][2]) + "\t" + str(scoreCollection[id][x][3]) + "\n")
        
        
    outFile.close()
