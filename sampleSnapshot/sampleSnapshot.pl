#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <directory to sample> <outdir> <amount to take (B/K/M/G)>
# ex.
# perl scriptname.pl folder/ /home/martin/sampleOut 100K

use warnings;
use strict;
use File::Path;
use Cwd;

=pod



=cut

# define messages
my $usage = "compress.pl usage:  perl scriptname.pl <directory to sample> <outdir> <amount to take (B/K/M/G)>\n";

# get the file names
my $folder = shift or die $usage;
my $out = shift or die $usage;
my $n = shift or die $usage;

# get sample size
my ($digit,$prefix) = $n =~ /^(\d+)(\w+)/;
if($prefix eq 'B'){
    $n = $digit;
}elsif($prefix eq 'K'){
    $n = 1024*$digit
}elsif($prefix eq 'M'){
    $n = 1024*1024*$digit
}elsif($prefix eq 'G'){
    $n = 1024*1024*1024*$digit
}else{
    die "Invalid prefix: $prefix\nValid prefixes: B, K, M, G\n";
}

# convert to an absolute path
$folder = absPath(remSlash($folder));
$out = absPath(remSlash($out));

# get a list of the files in the structure
open(FILES,"find \"$folder\" -type f | ") or die "Failed: $!\n";


# process each file
for my $file (<FILES>){

  # print status
  print "$file\n";
  
  # get the name
  chomp($file);
  #~ # escape the spaces in filenames
  #~ if($file =~ /\s/){
    #~ $file =~ s/\s/\\ /g;    
  #~ }
  
  # create the out directory
  #mkpath("$out/".remFirstSlash(getPath($file)));
  system("mkdir -p \"$out/".remFirstSlash(getPath($file))."\"");

  # write the first $n of the original file to the sample dir
  system("head --bytes=$n \"$file\" > \"$out/".remFirstSlash($file)."\"");
}




### METHODS ###

sub sepPath{ # get the path to a file (path/to/file.txt) and return the path and filename seperatly (file.txt,path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element first (the file name), and then the path
  return (pop(@arr),join("/",@arr));
}


sub getPath{ # get the path to a file (path/to/file.txt) and return the path (path/to)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  pop(@arr); # remove the file name
  # return the path
  return join("/",@arr);
}


sub getName{ # get the path to a file (path/to/file.txt) and return the filename (file.txt)
  # separate the string on the dashes
  my @arr = split(/\//,shift);
  
  # return the last element (the file name)
  return pop(@arr);
}


# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){
    chop($str);
  }
  return $str;
}


# Removes the / sign, if any, at the beginning of directory names
sub remFirstSlash{
  my $str = shift;
  my @arr;
  if($str =~ m/^\//){
    @arr = split(//,$str);
    shift(@arr);
    $str = join('',@arr);
  }
  return $str;
}


# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
