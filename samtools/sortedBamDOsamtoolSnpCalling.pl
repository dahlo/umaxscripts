#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <reference genome.fa> <infile> <outfile prefix>
# ex.
# perl scriptname.pl hg19.fa aln.sorted.bam out

use warnings;
use strict;

=pod

=cut

# paths
my $scripts = "/bubo/home/h18/dahlo/glob/work/uppmaxScripts";

# define messages
my $usage = "Usage:  perl scriptname.pl <reference genome.fa> <infile.bam> <outfile prefix>\n";

# get the file names
my $ref = shift or die $usage;
my $infile = shift or die $usage;
my $outfile = shift or die $usage;

# do the pileup
system("samtools pileup -vcf $ref $infile > $outfile.raw.pileup");

# filter the pileup
system("samtools.pl varFilter -D1000000 $outfile.raw.pileup | awk '\$6>=20' > $outfile.final.pileup");
