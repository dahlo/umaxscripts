#!/usr/bin/python


import sys


# get arguments
expired_file = sys.argv[1]
storage_file = sys.argv[2]


expired={}
# read the project information
for line in open(expired_file, 'r'):

	# separate columns
	line = line.rstrip().split('\t')

	expired[line[0]] = line[1:]



storage={}
# read the project information
for line in open(storage_file, 'r'):

	# skip comments and empty lines
	if line[0] == '#' or line == '\n':
		continue

	# separate columns
	line = line.rstrip().split()

	# create the entry if needed
	if line[0] not in storage:
		storage[line[0]] = {}


	# get the setting and save it
	setting, val = line[1].split('=')
	storage[line[0]][setting] = val


for proj in expired:

	if proj not in storage:
		# print proj
		pass

	else:
		print storage[proj]
		print expired[proj]
		print "\n"

	