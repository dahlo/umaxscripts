#!/usr/bin/python


import sys


# get arguments
zero_file = sys.argv[1]
storage_file = sys.argv[2]


zero={}
# read the project information
for line in open(zero_file, 'r'):

	# separate columns
	line = line.rstrip().split('\t')

	zero[line[0]] = line[1:]



storage={}
# read the project information
for line in open(storage_file, 'r'):

	# skip comments and empty lines
	if line[0] == '#' or line == '\n':
		continue

	# separate columns
	line = line.rstrip().split()

	# create the entry if needed
	if line[0] not in storage:
		storage[line[0]] = {}


	# get the setting and save it
	setting, val = line[1].split('=')
	storage[line[0]][setting] = val


for proj in zero:

	# if the project has been modified
	if proj in storage:
		
		
		if 'dir_nobackup' not in storage[proj]:
			print zero[proj]


	# if the project has not been modified
	else: 

		print zero[proj]

	