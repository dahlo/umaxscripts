#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl supportPipe.pl <config file>
# ex.
# perl 

use warnings;
use strict;
use Cwd;
use File::Path;
use Time::Local;

=pod


=cut

# define messages
my $usage = "Usage: perl supportPipe.pl <config file>\n";
my $confFileExp = "Please create your own run file with information that is needed to write a submission file to the SLURM system on the cluster.\n\nExample of a run file:\n \n";

# read the arguments
my $confFile = shift or die $usage;
my $scripts = shift or die $usage;




#############################################
#######   GET VARIABLES, BEGINNING   ########
#############################################

# open file handles
open CONF, "<", $confFile or die "Unable to open config file: $confFile\n$!";

# read the config file
my %opt;
while (<CONF>) {
  chomp;                  # no newline
  s/#.*//;                # no comments
  s/^\s+//;               # no leading white
  s/\s+$//;               # no trailing white
  next unless length;     # anything left?
  my ($var, $value) = split(/\s*=\s*/, $_, 2); # separate with a equal sign
  $opt{$var} = $value;
}

# check that all required options are entered. How boring..
if(!(defined $opt{'reads'}) || ($opt{'reads'} eq '')){ die "$confFileExp\n\nRead files required!\n\n"; }
if(!(defined $opt{'quals'}) || ($opt{'quals'} eq '')){ die "$confFileExp\n\nQuality files required!\n\n"; }
if(!(defined $opt{'out'}) || ($opt{'out'} eq '')){ die "$confFileExp\n\nOutput directory required!\n\n"; }

# save hash values to variable names for a more beautiful code further on
my $reads = $opt{'reads'};
my $quals = $opt{'quals'};
my $out = $opt{'out'};

#############################################
#######   GET VARIABLES, ENDING      ########
#############################################






#### CHECK FOR MULTIPLE SAMPLES ####
my @samples = split(/,/,$reads);  # split on comma signs
my $nrSamples = $#samples + 1;  # compensate for perls zero-starting array index..


#### FOR EACH SAMPLE BEGIN ####

my $globalCollect = "";  # collect all job IDs for the jobs ending the chain
my $prevCollect;  # save the ID of the previous job
for(my $i = 0; $i <= $#samples; $i++){
  
  # check if the files even exists before proceeding
  unless(-e $samples[$i]){
    die "solid.wtse.pl error: Reads file missing! ($samples[$i])\n";
  }
  
  

  #### RECALIBRATION ####
  # add later if needed

  
  #### TOPHAT ####
  # check if the .finished file exists
  unless(-e "$out/1TophatAlignment/$i/.finished"){  # only if .finished does not exist
  
    # send correct arguments to the tophat script and catches the job id
    #open(TOPHATID,"perl $scripts/tools/solid.wtse.tophat.pl  $proj $reads $ref $out $localCollect | ") or die $!;  # submit file and save response     # if recalibration is added
    open(TOPHATID,"perl $scripts/tools/solid.wtse.tophat.pl $confFile $scripts ".($i+1)." | ") or die $!;  # submit file and save response
    
    # get the job id and add to collectors
    <TOPHATID> =~ /Submitted batch job (\d+)/;  # get the job id
    $prevCollect = $1;  # save this job id for the next step in the chain
    close(TOPHATID);
    
  }else{ print "Skipping TopHat stage, since $out/1TophatAlignment/$i/.finished exists!\n"; }
  
  
  #### CUFFLINKS ####
  unless(-e "$out/2Cufflinks/$i/.finished"){  # only if .finished does not exist
  
    # send correct arguments to the cufflinks script
    open(CUFFID,"perl $scripts/tools/solid.wtse.cufflinks.pl $confFile $scripts ".($i+1)." --dependency=afterok:$prevCollect | ") or die $!;  # submit file and save response
    
    # get the job id and add to collectors
    <CUFFID> =~ /Submitted batch job (\d+)/;  # get the job id
    $globalCollect .= "$1:";  # save to the global collector, since it is the last step in the chain
    #$prevCollect = $1;  # save this job id for the next step in the chain
    close(CUFFID);
    
  }else{ print "Skipping CuffLinks stage, since $out/2Cufflinks/$i/.finished exists!\n"; }
}  

chop($globalCollect);  # remove the last comma sign

#### FOR EACH SAMPLE END ####









#### FOR ALL SAMPLES: CUFFCOMPARE AND CUFFDIFF ####

#### CUFFCOMPARE ####
unless(-e "$out/3Cuffcompare/.finished"){  # only if .finished does not exist

  # send correct arguments to the cufflinks script
  open(CUFFID,"perl $scripts/tools/solid.wtse.cuffcompare.pl $confFile $scripts --dependency=afterok:$globalCollect | ") or die $!;  # submit file and save response
  
  # get the job id and add to collectors
  <CUFFID> =~ /Submitted batch job (\d+)/;  # get the job id
  $prevCollect = $1;  # save the job id for the next step in the chain
  close(CUFFID);
  
}else{ print "Skipping Cuffcompare stage, since $out/3Cuffcompare/.finished exists!\n"; }


#### CUFFDIFF ####
unless(-e "$out/4Cuffdiff/.finished"){  # only if .finished does not exist
  
  # send correct arguments to the cuffdiff script
  open(CUFFID,"perl $scripts/tools/solid.wtse.cuffdiff.pl $confFile $scripts --dependency=afterok:$prevCollect | ") or die $!;  # submit file and save response
  
  # get the job id and add to collectors
  <CUFFID> =~ /Submitted batch job (\d+)/;  # get the job id
  $prevCollect = $1;  # save the job id for the next step in the chain
  close(CUFFID);
  
}else{ print "Skipping Cuffdiff stage, since $out/4Cuffdiff/.finished exists!\n"; }



### Methods

# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){ # if the last char is a /
    chop($str);
  }
  return $str;
}

# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
