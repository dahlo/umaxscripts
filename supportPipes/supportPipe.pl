#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl supportPipe.pl <config file>
# ex.
# perl 

use warnings;
use strict;
use Cwd;
use File::Path;
use Time::Local;

=pod

Pseudo code:
* Get all the arguments
* Write a submission file for SLURM using the given arguments
(* Submit the file to the queue system)
=cut

### HARDCODED ###  # location of the supportPipe files, will be passed on to all other scripts
my $scripts = "/home/dahlo/glob/work/uppmaxScripts/supportPipes";

# define messages
my $usage = "Usage: perl supportPipe.pl <config file>\n";
my $confFileExp = "Please create your own run file with information that is needed to write a submission file to the SLURM system on the cluster.\n\nExample of a run file:\n \n";

#############################################
#######   GET VARIABLES, BEGINNING   ########
#############################################

# read the arguments
my $confFile = shift or die $usage;
if($confFile eq '-h'){ die $confFileExp; }  # check for help flag

# Should maybe add a step that rewrites the conf file, removing all possible windows newline characters. Later.. If needed..

# open file handles
open CONF, "<", $confFile or die "Unable to open config file: $confFile\n$!";

# read the config file
my %opt;
while (<CONF>) {
  chomp;                  # no newline
  s/#.*//;                # no comments
  s/^\s+//;               # no leading white
  s/\s+$//;               # no trailing white
  next unless length;     # anything left?
  my ($var, $value) = split(/\s*=\s*/, $_, 2); # separate with a equal sign
  $opt{$var} = $value;
}

# check that all required options are entered. How boring..
if(!(defined $opt{'pipe'}) || ($opt{'pipe'} eq '')){ die "$confFileExp\n\nPipeline name required!\n\n"; }
if(!(defined $opt{'dataType'}) || ($opt{'dataType'} eq '')){ die "$confFileExp\n\nData type required!\n\n"; }

# save hash values to variable names for a more beautiful code further on
my $pipe = lc($opt{'pipe'});
my $dataType = lc($opt{'dataType'});

#############################################
#######   GET VARIABLES, FINISHED    ########
#############################################









#############################################
####  SEND TO CORRECT PIPE, BEGINNING    ####
#############################################



# check which pipe
if($pipe eq 'wtpe'){  # whole transcriptome paired end
  
  # check for the data type
  if($dataType eq 'illumina'){
    # launch the illumina wtpe pipeline and give it the correct info
    system("perl $scripts/pipes/illumina.wtpe.pl $confFile $scripts");
    
  }elsif($dataType eq 'solid'){
    # launch the illumina wtpe pipeline and give it the correct info
    system("perl $scripts/pipes/solid.wtpe.pl $confFile $scripts");
  
  }else{
    # unknown data type
    die "Error: unknown data type \"$dataType\"\n";    
    
  }
}elsif($pipe eq 'wtse'){  # whole transcriptome single end
  
  # check for the data type
  if($dataType eq 'illumina'){
    # launch the illumina wtpe pipeline and give it the correct info
    system("perl $scripts/pipes/illumina.wtse.pl $confFile $scripts");
    
  }elsif($dataType eq 'solid'){
    # launch the illumina wtpe pipeline and give it the correct info
    system("perl $scripts/pipes/solid.wtse.pl $confFile $scripts");
  
  }else{
    # unknown data type
    die "Error: unknown data type \"$dataType\"\n";    
    
  }
  
  }elsif($pipe eq 'dnamate'){  # whole transcriptome single end
    
    # check for the data type
    if($dataType eq 'illumina'){
      # launch the illumina wtpe pipeline and give it the correct info
      system("perl $scripts/pipes/illumina.dnamate.pl $confFile $scripts");
      
    }elsif($dataType eq 'solid'){
      # launch the illumina wtpe pipeline and give it the correct info
      system("perl $scripts/pipes/solid.dnamate.pl $confFile $scripts");
    
    }else{
      # unknown data type
      die "Error: unknown data type \"$dataType\"\n";    
      
    }
    
  }else{
  # unknown pipe
  die "Error: unknown pipeline \"$pipe\"\n";
  
}




#############################################
####   SEND TO CORRECT PIPE, FINISHED    ####
#############################################










### Methods

# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){ # if the last char is a /
    chop($str);
  }
  return $str;
}

# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
