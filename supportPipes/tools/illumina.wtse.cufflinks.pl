#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl supportPipe.pl <config file>
# ex.
# perl 

use warnings;
use strict;
use Cwd;
use File::Path;
use Time::Local;

=pod


=cut

# define messages
my $usage = "Usage: perl supportPipe.pl <config file>\n";
my $confFileExp = "Please create your own run file with information that is needed to write a submission file to the SLURM system on the cluster.\n\nExample of a run file:\n \n";

# read the arguments
my $confFile = shift or die $usage;
my $scripts = shift or die $usage;
my $fileId = shift or die "illumina.wtse.cufflinks.pl Error: Internal file ID missing in function call!\n";
my $depend = shift;
$fileId = $fileId - 1;  # can't send 0 as argument in perl.. so shifting back one step
#my $localCollect = shift or die "illumina.wtpe.cufflinks.pl Error: Job dependencies missing in function call!\n";


## read the path file
# open file handles
open PATHS, "<", "$scripts/path.txt" or die "Unable to open path file: $scripts/path.txt\n$!";
# read the path file
my %paths;
while (<PATHS>) {
  chomp;                  # no newline
  s/#.*//;                # no comments
  s/^\s+//;               # no leading white
  s/\s+$//;               # no trailing white
  next unless length;     # anything left?
  my ($var, $value) = split(/\s*=\s*/, $_, 2); # separate with a equal sign
  $paths{$var} = $value;
}




#############################################
#######   GET VARIABLES, BEGINNING   ########
#############################################

# open file handles
open CONF, "<", $confFile or die "Unable to open config file: $confFile\n$!";

# read the config file
my %opt;
while (<CONF>) {
  chomp;                  # no newline
  s/#.*//;                # no comments
  s/^\s+//;               # no leading white
  s/\s+$//;               # no trailing white
  next unless length;     # anything left?
  my ($var, $value) = split(/\s*=\s*/, $_, 2); # separate with a equal sign
  $opt{$var} = $value;
}


# check if a reference genome package name is defined
my $ref;
my $index;
my $filter;
if(!(defined $opt{'refPack'}) || ($opt{'refPack'} eq '')){  # if no package is defined, the user must specify each element explicitly
  if(!(defined $opt{'ref'}) || ($opt{'ref'} eq '')){ die "$confFileExp\n\nReference genome, or reference genome package, required!\n\n"; }else{ $ref = $opt{'ref'}; }  # assign specified value if definen
  if(!(defined $opt{'bowtieIndex'}) || ($opt{'bowtieIndex'} eq '')){ die "$confFileExp\n\nBowtie index path and prefix, or reference genome package, required!\n\n"; }else{ $index = $opt{'bowtieIndex'}; }  # assign specified value if definen
  if(!(defined $opt{'filter'}) || ($opt{'filter'} eq '')){ die "$confFileExp\n\nFilter GTF file required!\n\n"; }else{ $filter = $opt{'filter'}; }  # assign specified value if defined
  
}else{  # if a reference genome package is defined

  if(lc($opt{'refPack'}) eq 'hg19'){  # if HG19 is requested
    $ref = $paths{'hg19ref'};
    $index = $paths{'hg19index'};
    $filter = $paths{'hg19filter'};
    
  }else{  # unknown package name
    die "$confFileExp\n\nUnknown reference genome package name! (refPack = $opt{'refPack'})!\n\n";
  }
}

# check that all required options are entered. How boring..
if(!(defined $opt{'proj'}) || ($opt{'proj'} eq '')){ die "$confFileExp\n\nProject ID required!\n\n"; }
if(!(defined $opt{'reads'}) || ($opt{'reads'} eq '')){ die "$confFileExp\n\nRead files required!\n\n"; }
if(!(defined $opt{'out'}) || ($opt{'out'} eq '')){ die "$confFileExp\n\nOutput directory required!\n\n"; }

# save hash values to variable names for a more beautiful code further on
my $proj = $opt{'proj'};
my $reads = $opt{'reads'};
my $out = absPath($opt{'out'});

# check if the files exist
unless(-e $ref){
  die "illumina.wtse.cufflinks.pl error: Reference genome file missing! ($ref)\n";
}
unless(-e "$index.1.ebwt"){
  die "illumina.wtse.cufflinks.pl error: Bowtie index file missing! ($index.1.ebwt)\n";
}
unless(-e $filter){
  die "illumina.wtse.cufflinks.pl error: Filter GTF file missing! ($filter)\n";
}
$filter = "-M $filter";
#############################################
#######   GET VARIABLES, ENDING      ########
#############################################

# construct the folder name
my $folderName = $fileId;


# create and submit the job
system("mkdir -p $out/2Cufflinks/$folderName/");  # create the output dir
open SBATCH, ">", "$out/2Cufflinks/$folderName/cufflinks.sbatch" or die $!;
print SBATCH <<EOF;
#! /bin/bash -l
#SBATCH -A $proj
#SBATCH -p node -n 8
#SBATCH -t 30:00:00
#SBATCH -J CuffLinks:$folderName
#SBATCH -o $out/2Cufflinks/$folderName/run.out
#SBATCH -e $out/2Cufflinks/$folderName/run.err

# load the correct modules
module load bioinfo-tools
module load samtools/0.1.12-10  # this makes sure that always the same version is loaded, regardless of which is the default version at Uppmax

# run with a custom version of cufflinks
PATH=\$PATH:$paths{'cufflinks'}

# go to the output directory
cd $out/2Cufflinks/$folderName

# launch cufflinks with the supplied arguments
cufflinks -p 8 -o $out/2Cufflinks/$folderName -r $ref $filter $out/1TophatAlignment/$folderName/tophat_out/$folderName.bam

# rename the produced gtf file
mv $out/2Cufflinks/$folderName/transcripts.gtf $out/2Cufflinks/$folderName/$folderName.gtf

# signal that the script has finished
touch .finished
EOF

# submit the file
system("sbatch $depend $out/2Cufflinks/$folderName/cufflinks.sbatch");
#print "Submitted batch job $folderName\n";

### Methods

# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){ # if the last char is a /
    chop($str);
  }
  return $str;
}

# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
