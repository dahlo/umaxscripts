#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage: perl supportPipe.pl <config file>
# ex.
# perl 

use warnings;
use strict;
use Cwd;
use File::Path;
use Time::Local;

=pod


=cut

# define messages
my $usage = "Usage: perl supportPipe.pl <config file>\n";
my $confFileExp = "Please create your own run file with information that is needed to write a submission file to the SLURM system on the cluster.\n\nExample of a run file:\n \n";

# read the arguments
my $confFile = shift or die $usage;
my $scripts = shift or die $usage;
my $depend = shift;


## read the path file
# open file handles
open PATHS, "<", "$scripts/path.txt" or die "Unable to open path file: $scripts/path.txt\n$!";
# read the path file
my %paths;
while (<PATHS>) {
  chomp;                  # no newline
  s/#.*//;                # no comments
  s/^\s+//;               # no leading white
  s/\s+$//;               # no trailing white
  next unless length;     # anything left?
  my ($var, $value) = split(/\s*=\s*/, $_, 2); # separate with a equal sign
  $paths{$var} = $value;
}


#############################################
#######   GET VARIABLES, BEGINNING   ########
#############################################

# open file handles
open CONF, "<", $confFile or die "Unable to open config file: $confFile\n$!";

# read the config file
my %opt;
while (<CONF>) {
  chomp;                  # no newline
  s/#.*//;                # no comments
  s/^\s+//;               # no leading white
  s/\s+$//;               # no trailing white
  next unless length;     # anything left?
  my ($var, $value) = split(/\s*=\s*/, $_, 2); # separate with a equal sign
  $opt{$var} = $value;
}


my $ref;
my $annot;
# check if a reference genome package name is defined
if(!(defined $opt{'refPack'}) || ($opt{'refPack'} eq '')){  # if no package is defined, the user must specify each element explicitly
  if(!(defined $opt{'annot'}) || ($opt{'annot'} eq '')){ die "$confFileExp\n\nAnnotations for the reference genome GTF file required!\n\n"; }else{ $annot = "$opt{'annot'}"; }  # assign specified value if defined
  
}else{  # if a reference genome package is defined

  if(lc($opt{'refPack'}) eq 'hg19'){  # if hg19 is requested, assign default values
    $annot = $paths{'hg19annot'};
    
  }else{  # unknown package name
    die "$confFileExp\n\nUnknown reference genome package name! (refPack = $opt{'refPack'})!\n\n";
  }
}

# check that all required options are entered. How boring..
if(!(defined $opt{'proj'}) || ($opt{'proj'} eq '')){ die "$confFileExp\n\nProject ID required!\n\n"; }
if(!(defined $opt{'reads'}) || ($opt{'reads'} eq '')){ die "$confFileExp\n\nRead files required!\n\n"; }
if(!(defined $opt{'out'}) || ($opt{'out'} eq '')){ die "$confFileExp\n\nOutput directory required!\n\n"; }

# save hash values to variable names for a more beautiful code further on
my $proj = $opt{'proj'};
my $reads = $opt{'reads'};
my $out = absPath($opt{'out'});

# check if the files exist
unless(-e $annot){
  die "solid.wtpe.cuffcompare.pl error: Annotations for the reference genome GTF file missing! ($annot)\n";
}

#############################################
#######   GET VARIABLES, ENDING      ########
#############################################


# generate a list with all the gtf files from the cufflinks runs
my @samples = split(/,/,$reads);  # split on comma signs
my $gtfs = "";  # collect all the gtf files
for(my $i = 0; $i <= $#samples; $i = $i+2){
  $gtfs .= "$out/2Cufflinks/$i-".($i+1)."/$i-".($i+1).".gtf ";  # add the file to the list
}


# create and submit the job
system("mkdir -p $out/3Cuffcompare");  # create the output dir
open SBATCH, ">", "$out/3Cuffcompare/cuffcompare.sbatch" or die $!;
print SBATCH <<EOF;
#! /bin/bash -l
#SBATCH -A $proj
#SBATCH -p core
#SBATCH -t 5:00:00
#SBATCH -J Cuffcompare
#SBATCH -o $out/3Cuffcompare/run.out
#SBATCH -e $out/3Cuffcompare/run.err


# load the correct modules
module load bioinfo-tools
module load samtools/0.1.12-10  # this makes sure that always the same version is loaded, regardless of which is the default version at Uppmax

# run with a custom version of cufflinks
PATH=\$PATH:$paths{'cufflinks'}

# go to the output directory
cd $out/3Cuffcompare

# launch cuffcompare with the supplied arguments
cuffcompare -o $out/3Cuffcompare/out -r $annot $gtfs

# signal that the script has finished
touch .finished
EOF

# submit the file
system("sbatch $depend $out/3Cuffcompare/cuffcompare.sbatch");
#print "Submitted batch job $folderName\n";

### Methods

# Removes the / sign, if any, at the end of directory names
sub remSlash{
  my $str = shift;
  if($str =~ m/\/$/){ # if the last char is a /
    chop($str);
  }
  return $str;
}

# Adjust relative path to absolute paths
sub absPath{
  my $str = shift;
  if($str !~ m/^\//){ # if the first char is not a / ie. a relative path
    $str = &Cwd::cwd()."/$str"; # attach the current work directory infront of the relative path
  }
  return $str;  
}
