#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <cuffdiff result file> <outfile> <link to redirect script>
# ex.
# perl scriptname.pl isoform_fpkm.tracking isoform_fpkm.tracking.URLs https://export.uppmax.uu.se/b2010074/files/ucscVis/2011-04-20-13-15-53

use warnings;
use strict;
use POSIX qw/ceil/;
sub max ($$) { $_[$_[0] < $_[1]] }  # max
sub min ($$) { $_[$_[0] > $_[1]] }  # min  (suprise.. and no, i did not write them myself..)

=pod

=cut

# define messages
my $usage = "Usage:  perl scriptname.pl <cuffdiff result file> <outfile> <link to redirect script>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;
my $preLink = shift or die $usage;

#print $link;


# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", $outfile or die $!;

# add element to header
my $head = <IN>; # get the header
my ($pre, $post) = ($head =~ /^(\S+\t\S+)\t(.+)$/);
$head = "$pre\tUcscLink\t$post";

my $locus;
my $line;
while(<IN>){
  
  if($_ =~ /^(\S+\t\S+\t\S+\t\S+\t\S+\t)(\S+)(.+)$/){  # if it is a valid row
    
    $pre = $1;
    $post = $3;
    $locus = $2;
    
    $locus =~ /^chr(\d+):(\d+)-(\d+)/;
    
    $line = "$pre=HYPERLINK(\"$preLink/$1.$2.$3\";\"UcscVis\")\t$locus$post\n";
    print $line;
    
  }
  
}
