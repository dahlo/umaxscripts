#!/usr/bin/perl
# Author: Martin Dahlo
#
# Usage:  perl scriptname.pl <chr\tstart\tstop list> <outfile> <link to redirect script>
# ex.
# perl scriptname.pl myPos.txt transLinks.txt http://array.medsci.uu.se/open/ucscVis/2011-02-28-15-39-10/redirect.php

use warnings;
use strict;
use POSIX qw/ceil/;
sub max ($$) { $_[$_[0] < $_[1]] }  # max
sub min ($$) { $_[$_[0] > $_[1]] }  # min  (suprise.. and no, i did not write them myself..)

=pod

=cut

# define messages
my $usage = "Usage:  perl scriptname.pl <chr\\tstart\\tstop list> <outfile> <link to redirect script>\n";

# get the file names
my $infile = shift or die $usage;
my $outfile = shift or die $usage;
my $link = shift or die $usage;

#print $link;


# open file handles
open IN, "<", $infile or die $!;
open OUT, ">", $outfile or die $!;

<IN>; # remove the header
while(<IN>){
  
  # pick out the needed values
  if($_ =~ m/chr(\d+):(\d+)-(\d+)/){
    
  # get the start and stop of the transcript and add some padding
  my $start = min($2,$3);
  my $stop = max($2,$3);
  
  #print "$start\t$stop\:\t";
  
  my $frac = 1;
  my $diff = $stop-$start;
  $start -= ceil($diff * $frac);  # create $frac % padding
  $stop += ceil($diff * $frac);  # create $frac % padding
  
  #print "$start\t$stop\n";
    
    print OUT "=HYPERLINK(\"$link?chr=$1&start=$start&stop=$stop\";\"UcscVis\")\n";
    
  }
}
